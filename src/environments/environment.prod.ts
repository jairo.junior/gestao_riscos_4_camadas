import { ProtocolType } from './../app/shared/models/enums/protocol-type.enum';

export const environment = {
  production: true,

  // protocol: ProtocolType.scorm,
  protocol: ProtocolType.none,
  viewedScreensAutoCompleteValue: 0
};
