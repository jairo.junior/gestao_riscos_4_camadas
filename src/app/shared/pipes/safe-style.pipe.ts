import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safeStyle'
})
export class SafeStylePipe implements PipeTransform {
  /**
   * Construtor.
   * @param {DomSanitizer} sanitized
   * @memberof SafeHtmlPipe
   */
  constructor(private sanitized: DomSanitizer) { }

  /**
   * Converte os estilos para formato "confiável".
   * @param {*} value Estilos que serão convertido.
   * @returns Estilos convertidos.
   * @memberof SafeHtmlPipe
   */
  transform(value) {
    return this.sanitized.bypassSecurityTrustStyle(value);
  }
}
