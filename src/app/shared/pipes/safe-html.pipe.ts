import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safeHtml'
})
export class SafeHtmlPipe implements PipeTransform  {
  /**
   * Construtor.
   * @param {DomSanitizer} sanitized
   * @memberof SafeHtmlPipe
   */
  constructor(private sanitized: DomSanitizer) { }

  /**
   * Converte o texto HTML para formato "confiável".
   * @param {*} value HTML que será convertido.
   * @returns HTML convertido.
   * @memberof SafeHtmlPipe
   */
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
