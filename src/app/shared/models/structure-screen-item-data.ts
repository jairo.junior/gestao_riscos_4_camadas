import { Type } from '@angular/core';

import { ScreenItem } from '../base/screen-item.component';

/**
 * Entidade responsável pela configuração de cada item das telas.
 * @export
 * @class StructureScreenItemData
 */
export class StructureScreenItemData {
    /**
     * Tipo do componente representado pelo item.
     * @type {Type<any>}
     * @memberof StructureScreenItemData
     */
    type: Type<ScreenItem>;

    /**
     * Classes CSS aplicadas no elemento.
     * @type {string}
     * @memberof StructureScreenItemData
     */
    classes: string;

    /**
     * Estilos CSS aplicadas no elemento.
     * @type {string}
     * @memberof StructureScreenItemData
     */
    styles?: string;

    /**
     * Dados enviados ao componente.
     * @type {*}
     * @memberof StructureScreenItemData
     */
    data: any;

    /**
     * Define quantos itens contando a partir do item atual serão ocultos quando a interação do item acabar.
     * @type {number}
     * @memberof StructureScreen
     */
    hideCurrentAndPreviousOnFinishCount?: number;
}
