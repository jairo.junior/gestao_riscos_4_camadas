import { StructureScreen } from './structure-screen.model';

/**
 * Entidade que representa um tópico do curso.
 * @export
 * @class StructureScreen
 */
export class StructureTopic {
    /**
     * Título do tópico.
     * @type {string}
     * @memberof StructureTopic
     */
    title: string;

    /**
     * Lista de telas disponíveis no tópico.
     * @type {Array<StructureScreen>}
     * @memberof StructureTopic
     */
    screens: Array<StructureScreen>;
}
