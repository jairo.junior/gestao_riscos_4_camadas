/**
 * Status de andamento do curso.
 * @export
 * @enum {number}
 */
export enum ProtocolStatus {
    /**
     * Tópico não inicializado.
     */
    notStarted = 0,

    /**
     * Tópico em andamento.
     */
    started = 1,

    /**
     * Tópico finalizado.
     */
    completed = 2,
}
