/**
 * Tipo de protocolo de comunicação.
 * @export
 * @enum {number}
 */
export enum ProtocolType {
    /**
     * Sem utilização de protocolo de comunicação.
     */
    none = 0,

    /**
     * Padrão Scorm 1.2 de comunicação.
     */
    scorm = 1
}
