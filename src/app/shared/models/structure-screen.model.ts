import { StructureScreenItemData } from './structure-screen-item-data';

/**
 * Entidade que representa uma tela do curso.
 * @export
 * @class StructureScreen
 */
export class StructureScreen {
    /**
     * Título da tela.
     * @type {string}
     * @memberof StructureScreen
     */
    title: string;

    /**
     * Define se o item é exibido no menu.
     * @type {boolean}
     * @memberof StructureScreen
     */
    showInMenu: boolean;

    /**
     * Título do tópico da tela.
     * @type {string}
     * @memberof StructureScreen
     */
    topicTitle: string;

    /**
     * Classes aplicadas ao fundo aplicado a tela.
     * @type {string}
     * @memberof StructureScreen
     */
    backgroundClasses?: string;

    /**
     * Cor de fundo aplicada a tela.
     * @type {string}
     * @memberof StructureScreen
     */
    backgroundColor?: string;

    /**
     * Imagem de fundo aplicada a tela.
     * @type {string}
     * @memberof StructureScreen
     */
    backgroundImage: string;

    /**
     * Classes aplicadas ao container da tela.
     * @type {string}
     * @memberof StructureScreen
     */
    screenClasses: string;

    /**
     * Lista de itens renderizados na tela.
     * @type {Array<ScreenItem>}
     * @memberof StructureScreen
     */
    items: Array<StructureScreenItemData>;

    /**
     * Define se a tela já foi concluída.
     * @type {boolean}
     * @memberof StructureScreen
     */
    completed: boolean;
}
