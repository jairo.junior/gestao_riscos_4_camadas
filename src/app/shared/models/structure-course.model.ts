import { StructureTopic } from './structure-topic.model';

/**
 * Entidade que representa o curso.
 * @export
 * @class StructureScreen
 */
export class StructureCourse {
    /**
     * Título do curso.
     * @type {string}
     * @memberof StructureCourse
     */
    title: string;

    /**
     * Lista de tópicos do curso.
     * @type {Array<StructureTopic>}
     * @memberof StructureCourse
     */
    topics: Array<StructureTopic>;
}
