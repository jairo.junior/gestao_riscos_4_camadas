import { EventEmitter, HostBinding, Input, Output } from '@angular/core';

import { fadeAnimation } from './../animations/animations';

/**
 * Elemento base utilizado para criação de itens das telas.
 * @export
 * @class ScreenItem
 */
export abstract class ScreenItem {
    @HostBinding('@fadeAnimation')
    get fadeAnimation() {
        return fadeAnimation;
    }


    /**
     * Define se o item está finalizado.
     * @type {boolean}
     * @memberof ScreenItem
     */
    isFinished: boolean;

    /**
     * Evento disparado quando o item é finalizado.
     * @type {EventEmitter}
     * @memberof ScreenItem
     */
    @Output()
    finished: EventEmitter<object>;


    /**
     * Construtor.
     * @memberof ScreenItem
     */
    constructor() {
        this.isFinished = false;
        this.finished = new EventEmitter();
    }


    /**
     * Dados enviados para funcionamento do componente.
     * @memberof ScreenItem
     */
    @Input()
    set data(value: any) {
        this.parseData(value);
    }

    /**
     * Formata os dados recebidos na criação.
     * @param {*} value Valores recebidos na criação.
     * @memberof ScreenItem
     */
    parseData(value: any) {

    }

    /**
     * Inicia a execução do item.
     * @memberof ScreenItem
     */
    start() { }

    /**
     * Finaliza a execução do item e notifica aos ouvintes.
     * @memberof ScreenItem
     */
    finish(advance: boolean = false) {
        if (!this.isFinished) {
            this.finished.emit({ advance: advance });
        }

        this.isFinished = true;
    }
}
