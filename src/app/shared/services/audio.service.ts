import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/**
 * Serviços responsáveis pela reprodução de áudios.
 * @export
 * @class AudioService
 */
export class AudioService {
  /**
   * Define se áudios estão ativos no curso.
   * @type {boolean}
   * @memberof AudioService
   */
  isActive: boolean;

  /**
   * Define se existe áudio em reprodução.
   * @type {boolean}
   * @memberof AudioService
   */
  playing: boolean;


  /**
   * Player de áudios.
   * @private
   * @type {HTMLAudioElement}
   * @memberof AudioService
   */
  private _audio: HTMLAudioElement;


  /**
   * Evento que notifica início da reprodução do áudio.
   * @type {EventEmitter<object>}
   * @memberof AudioService
   */
  started: EventEmitter<object>;

  /**
   * Evento que notifica término da reprodução do áudio.
   * @type {EventEmitter<object>}
   * @memberof AudioService
   */
  finished: EventEmitter<object>;


  /**
   * Construtor.
   * @memberof AudioService
   */
  constructor() {
    this.isActive = true;

    this.started = new EventEmitter();
    this.finished = new EventEmitter();
  }


  /**
   * Alterna o status de atividade do áudio.
   * @memberof AudioService
   */
  toggleIsActive() {
    this.isActive = !this.isActive;

    if (!this.isActive && this._audio !== null) {
      this.onAudioFinished();
      this.destroy();
    }
  }


  play(file: string) {
    if (!this.isActive) {
      this.onAudioFinished();
      return;
    }

    if (this._audio != null) {
      if (this._audio.paused) {
        this._audio.play();

        this.playing = true;
      } else {
        this.destroy();
      }
    } else {
      this._audio = new Audio();
      this._audio.src = file;
      this._audio.addEventListener('playing', () => { this.onAudioStarted(); });
      this._audio.addEventListener('ended', () => { this.onAudioFinished(); });
      this._audio.load();
      this._audio.play();

      this.playing = true;
    }
  }

  pause() {
    if (this._audio == null) {
      return;
    }

    if (!this._audio.paused) {
      this._audio.pause();

      this.playing = false;
    }
  }

  stop() {
    if (this._audio == null) {
      return;
    }

    this._audio.pause();
    this._audio.currentTime = 0;

    this.playing = false;
  }

  destroy() {
    if (this._audio == null) {
      return;
    }

    this._audio.removeEventListener('ended', () => { this.onAudioFinished(); });
    this._audio.pause();
    this._audio = null;

    this.playing = false;
  }

  /**
   * Ouvinte que detecta início da reprodução para notificar clientes.
   * @private
   * @memberof AudioService
   */
  private onAudioStarted(): void {
    this.started.emit({});
  }

  /**
   * Ouvinte que monitora fim da reprodução dos áudios.
   * @private
   * @memberof AudioService
   */
  private onAudioFinished() {
    this.playing = false;

    this.finished.emit({});
  }
}
