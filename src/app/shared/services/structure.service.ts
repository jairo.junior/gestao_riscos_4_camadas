import { EventEmitter, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { environment } from '../../../environments/environment';
import { StructureCourse } from '../models/structure-course.model';
import { StructureScreen } from '../models/structure-screen.model';
import { StructureTopic } from './../models/structure-topic.model';
import { ProtocolService } from './protocol.service';
import { course } from './structure/course';
import { TimeoutService } from './timeout.service';


@Injectable({
  providedIn: 'root'
})

/**
 * Serviços responsáveis por disponibilizar a estrutura do curso.
 * @export
 * @class StructureService
 */
export class StructureService {
  /**
   * Estrutura do curso.
   * @private
   * @type {StructureTopic[]}
   * @memberof StructureService
   */
  private readonly structure: StructureCourse = course;


  /**
   * Define se os parâmetros estão inicializados.
   * @private
   * @type {boolean}
   * @memberof StructureService
   */
  private _paramsInitialized: boolean;


  /**
   * Posição do tópico em exibição na lista.
   * @private
   * @type {number}
   * @memberof StructureService
   */
  private _topicIndex: number;

  /**
   * Posição da tela em exibição na lista.
   * @private
   * @type {number}
   * @memberof StructureService
   */
  private _screenIndex: number;

  /**
   * Posição da tela em exibição na lista.
   * @private
   * @type {number}
   * @memberof StructureService
   */
  private _previousScreenIndex: number;

  /**
   * Evento que notifica modificação na tela.
   * @type {EventEmitter}
   * @memberof StructureService
   */
  screenChanged: EventEmitter<object>;


  /**
   * Construtor.
   * @param {ActivatedRoute} route Serviços disponibilizados para trabalhar com rotas.
   * @param {ProtocolService} protocolService Serviços responsáveis pela comunicação com a plataforma LMS.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof StructureService
   */
  constructor(/*private route: ActivatedRoute, */private protocolService: ProtocolService, private timeoutService: TimeoutService) {
    this.screenChanged = new EventEmitter();

    this._screenIndex = 0;
    this._previousScreenIndex = 0;
    /*
    setTimeout(() => {
      this.route.queryParams.subscribe(params => {
        if (this._paramsInitialized) {
          return;
        }
        console.log(params, this.route.snapshot.queryParams, this.route.snapshot.queryParamMap, this.route.snapshot.queryParamMap.get('topic'));
        this.parseCurrentTopic(params['topic']);
        this.parseProtocolViewedScreens(params['screen']);
  
        this._paramsInitialized = true;
      });
    }, 2000);
    */
    window['reviewScreen'] = this.review.bind(this);
  }


  /**
   * Inicializa o curso com a URL recebida.
   * @param {string} url
   * @memberof StructureService
   */
  initializeWith(url: string) {
    if (this._paramsInitialized) {
      return;
    }

    let query: string = url.indexOf('?') > 0 ? url.substring(url.indexOf('?') + 1) : null;
    let queryData = this.parseQuery(query || '');

    this.parseCurrentTopic(queryData.hasOwnProperty('topic') ? queryData['topic'] : 1);
    this.parseProtocolViewedScreens(queryData.hasOwnProperty('screen') ? queryData['screen'] : 1);

    this._paramsInitialized = true;
  }

  /**
   * Carrega valores da query string recebida.
   * @private
   * @param {string} queryString Query string que será quebrada.
   * @returns {*} Objeto construído com parâmetros.
   * @memberof StructureService
   */
  private parseQuery(queryString: string): any {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}


  /**
   * Título do curso.
   * @readonly
   * @type {string}
   * @memberof StructureService
   */
  get title(): string {
    return this.structure.title;
  }

  /**
   * Tópico atual.
   * @readonly
   * @type {StructureTopic}
   * @memberof StructureService
   */
  get topic(): StructureTopic {
    return this.structure.topics[this._topicIndex];
  }

  /**
   * Posição do tópico atual.
   * @readonly
   * @type {number}
   * @memberof StructureService
   */
  get topicCount(): number {
    return this._topicIndex + 1;
  }

  /**
   * Tela atual.
   * @readonly
   * @type {StructureScreen}
   * @memberof StructureService
   */
  get screen(): StructureScreen {
    return this.topic ? this.topic.screens[this._screenIndex] : null;
  }


  /**
   * Carrega os tópicos referenciados nas telas.
   * @readonly
   * @type {Array<string>}
   * @memberof StructureService
   */
  get screenTopics(): Array<string> {
    return this.topic.screens.map(item => item.topicTitle).filter((value, index, self) => self.indexOf(value) === index);
  }

  /**
   * Número da tela atual.
   * @readonly
   * @type {number}
   * @memberof StructureService
   */
  get screenCount(): number {
    if (!isNaN(this._screenIndex)) {
      return this._screenIndex >= 0 ? this._screenIndex + 1 : this._previousScreenIndex + 1;
    }

    return 0;
  }

  /**
   * Número total de telas do tópico.
   * @readonly
   * @type {number}
   * @memberof StructureService
   */
  get totalScreensCount(): number {
    return this.topic ? this.topic.screens.length : 0;
  }


  /**
   * Progresso atual do usuário no tópico.
   * @readonly
   * @type {number}
   * @memberof StructureService
   */
  get progress(): number {
    try {
      return this.topic.screens.filter(x => x.completed).length / this.totalScreensCount * 100;
    } catch (error) {
      return 0;
    }
  }


  /**
   * Carrega as telas que devem ser exibidas no menu a partir do título do tópico.
   * @param {string} topicTitle Título do tópico.
   * @returns {Array<StructureScreen>} Lista de telas.
   * @memberof StructureService
   */
  getMenuScreensByTopic(topicTitle: string): Array<StructureScreen> {
    return this.topic.screens.filter((value, index, self) => value.topicTitle === topicTitle && value.showInMenu);
  }

  /**
   * Valida se a tela de menu recebida está concluída.
   * @param {StructureScreen} screen Tela que será validada.
   * @returns {boolean} Resultado da validação.
   * @memberof StructureService
   */
  isMenuContentCompleted(screen: StructureScreen): boolean {
    if (!screen.showInMenu) {
      return false;
    }
    
    let index = this.topic.screens.indexOf(screen);
    if (index < 0) { 
      return false;
    }
    
    for (let i = index + 1; i < this.topic.screens.length; i++) {
      const element = this.topic.screens[i];
      if (!element.completed && !element.showInMenu) {
        return false;
      }

      if (element.showInMenu) {
        break;
      }
    }
    
    return true;
  }

  /**
   * Carrega a posição da tela recebida na lista.
   * @param {StructureScreen} screen Tela que terá posição procurada.
   * @returns Posição da tela na lista.
   * @memberof StructureService
   */
  getScreenIndex(screen: StructureScreen) {
    return this.topic.screens.indexOf(screen);
  }


  /**
   * Valida se a tela da posição recebida pode ser acessada.
   * @param {number} value Número da tela que será validada.
   * @returns {boolean} Resultado da validação.
   * @memberof StructureService
   */
  canAccess(value: number): boolean {
    if (value < 1 || value > this.totalScreensCount) {
      return false;
    }

    const previousScreen = value - 2 >= 0 ? this.topic.screens[value - 2] : null;
    return previousScreen == null || previousScreen.completed;
  }

  /**
   * Valida se pode ser navegado para a próxima tela.
   * @returns {boolean}
   * @memberof StructureService
   */
  canGoNext(): boolean {
    return this.canAccess(this.screenCount + 1);
  }

  /**
   * Valida se pode ser navegado para a tela anterior.
   *
   * @returns {boolean}
   * @memberof StructureService
   */
  canGoBack(): boolean {
    return this.canAccess(this.screenCount - 1);
  }

  /**
   * Realiza o acesso a tela.
   * @param {number} value Tela que será acessada.
   * @memberof StructureService
   */
  tryAccess(value: number) {
    if (!this.canAccess(value)) {
      return;
    }

    this.updateScreenIndexAndNotify(value - 1);
  }

  /**
   * Tenta navegar para a próxima tela.
   * @memberof StructureService
   */
  tryGoNext() {
    this.tryAccess(this.screenCount + 1);
  }

  /**
   * Tenta navegar para a tela anterior.
   * @memberof StructureService
   */
  tryGoBack() {
    this.tryAccess(this.screenCount - 1);
  }

  /**
   * Recarrega a tela atual.
   * @memberof StructureService
   */
  review() {
    this.tryAccess(this.screenCount);
  }

  /**
   * Finaliza a tela atual.
   * @private
   * @memberof StructureService
   */
  finishCurrentScreen() {
    this.screen.completed = true;

    this.recordCurrentAndViewedScreens();
  }


  /**
   * Valida qual tópico deve ser utilizado baseado no valor da querystring recebido.
   * @private
   * @param {string} queryTopic Valor do tópico recebido na querystring.
   * @memberof StructureService
   */
  private parseCurrentTopic(queryTopic: string): void {
    const value: number = parseInt(queryTopic, 10);
    this._topicIndex = !isNaN(value) && value > 0 ? value - 1 : 0;

    if (this.structure.topics.length < this._topicIndex + 1) {
      this._topicIndex = 0;
    }
  }

  /**
   * Carrega as informações de status de visualização das telas.
   * @private
   * @param {string} queryScreen Valor da tela recebida na querystring.
   * @memberof StructureService
   */
  private parseProtocolViewedScreens(queryScreen: string): void {
    const viewedScreens = this.protocolService.getViewedScreens();

    // Removendo posições em excesso no array caso telas tenham sido removidas.
    while (this.totalScreensCount < viewedScreens.length) {
      viewedScreens.shift();
    }

    // Adicionando posições ao array caso telas tenham sido adicionadas.
    while (this.totalScreensCount > viewedScreens.length) {
      viewedScreens.push(environment.viewedScreensAutoCompleteValue);
    }

    for (let i = 0; i < viewedScreens.length; i++) {
      this.topic.screens[i].completed = viewedScreens[i] !== 0;
    }

    this.parseCurrentScreen(queryScreen);
  }

  /**
   * Valida qual tela deve ser utilizada baseado no valor da querystring recebido.
   * @private
   * @param {string} queryScreen Valor da tela recebida na querystring.
   * @memberof StructureService
   */
  private parseCurrentScreen(queryScreen: string): void {
    const parsedQueryValue: number = parseInt(queryScreen, 10);
    let parsedValue = !isNaN(parsedQueryValue) && parsedQueryValue > 0 ? parsedQueryValue : 1;
    parsedValue = Math.max(parsedValue, this.protocolService.getCurrentScreen());

    if (this.canAccess(parsedValue)) {
      this.tryAccess(parsedValue);
    } else {
      this.tryAccess(1);
    }
  }


  /**
   * Atualiza a tela atual e dispara evento notificando.
   * @private
   * @param {number} value Posição da tela na lista.
   * @memberof StructureService
   */
  private updateScreenIndexAndNotify(value: number) {
    this._previousScreenIndex = this._screenIndex;
    this._screenIndex = -1;
    
    this.timeoutService.set(() => {
      this._screenIndex = value;
    
      this.screenChanged.emit(this.screen);
  
      this.recordCurrentAndViewedScreens();
    }, 250);
  }

  /**
   * Realiza a gravação da tela atual e da lista de status de visualização das telas.
   * @private
   * @memberof StructureService
   */
  private recordCurrentAndViewedScreens() {
    const viewedScreens = new Array<number>();
    for (let i = 0; i < this.topic.screens.length; i++) {
      viewedScreens.push(this.topic.screens[i].completed ? 1 : 0);
    }

    this.protocolService.setCurrentScreen(this.screenCount);
    this.protocolService.setViewedScreens(viewedScreens);
  }
}
