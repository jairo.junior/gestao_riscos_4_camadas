import { StructureCourse } from '../../models/structure-course.model';
import { topic1 } from './topic1';

export const course: StructureCourse = {
    title: 'GESTÃO DE RISCOS – 4 CAMADAS',
    topics: [ topic1 ]
};
