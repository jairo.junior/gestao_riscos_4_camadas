import { StructureTopic } from '../../models/structure-topic.model';
import { topicTitles } from './topic-titles';
import { t1s1 } from './topic1/screen1';
import { t1s10 } from './topic1/screen10';
import { t1s11 } from './topic1/screen11';
import { t1s12 } from './topic1/screen12';
import { t1s13 } from './topic1/screen13';
import { t1s14 } from './topic1/screen14';
import { t1s15 } from './topic1/screen15';
import { t1s16 } from './topic1/screen16';
import { t1s17 } from './topic1/screen17';
import { t1s18 } from './topic1/screen18';
import { t1s19 } from './topic1/screen19';
import { t1s2 } from './topic1/screen2';
import { t1s20 } from './topic1/screen20';
import { t1s21 } from './topic1/screen21';
import { t1s22 } from './topic1/screen22';
import { t1s23 } from './topic1/screen23';
import { t1s24 } from './topic1/screen24';
import { t1s25 } from './topic1/screen25';
import { t1s26 } from './topic1/screen26';
import { t1s27 } from './topic1/screen27';
import { t1s28 } from './topic1/screen28';
import { t1s29 } from './topic1/screen29';
import { t1s3 } from './topic1/screen3';
import { t1s30 } from './topic1/screen30';
import { t1s31 } from './topic1/screen31';
import { t1s32 } from './topic1/screen32';
import { t1s33 } from './topic1/screen33';
import { t1s34 } from './topic1/screen34';
import { t1s35 } from './topic1/screen35';
import { t1s36 } from './topic1/screen36';
import { t1s37 } from './topic1/screen37';
import { t1s38 } from './topic1/screen38';
import { t1s39 } from './topic1/screen39';
import { t1s4 } from './topic1/screen4';
import { t1s40 } from './topic1/screen40';
import { t1s41 } from './topic1/screen41';
import { t1s5 } from './topic1/screen5';
import { t1s6 } from './topic1/screen6';
import { t1s7 } from './topic1/screen7';
import { t1s8 } from './topic1/screen8';
import { t1s9 } from './topic1/screen9';

export const topic1: StructureTopic = {
    title: topicTitles[0],
    screens: [
      t1s1, t1s2, t1s3, t1s4, t1s5, t1s6, t1s7, t1s8, t1s9, t1s10, t1s11, t1s12, t1s13, t1s14, t1s15, t1s16, t1s17, t1s18, t1s19, t1s20, t1s21, t1s22, t1s23, t1s24, t1s25, t1s26, t1s27, t1s28, t1s29, t1s30, t1s31, t1s32, t1s33, t1s34, t1s35, t1s36, t1s37, t1s38, t1s39, t1s40, t1s41
    ]
};
