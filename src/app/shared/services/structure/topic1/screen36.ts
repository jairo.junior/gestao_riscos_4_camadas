import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s36: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-10`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-10`,
            data: {
              charStyle: `full`,
              text: `Depois de todo esse procedimento na terceira camada, deve ser feito o monitoramento e a melhoria contínua, que dizem respeito a última etapa do processo de Elaboração do Procedimento Específico (PE).`,
              audio: `./assets/audios/screen_1_53_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-9 offset-2`,
      styles: `position: absolute; top: 285px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `ETAPA: Monitoramento e Melhoria Contínua.`,
              containerClasses: `ft-opensans-bold color-11`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-5`,
            data: {
              source: `./assets/images/screen_1_53_1.png`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-7`,
            data: {
              content: 
              `<p>Depois de implantado o procedimento específico, a sua aplicação e eficácia devem ser monitoradas por meio da Observação Planejada da Tarefa (OPT), conforme procedimento PN-0117.</p> 

              <p class="mb-0">O monitoramento da aderência ao procedimento e dos resultados das OPTs também devem ser realizados conforme o PN-0117.</p>`,
              containerClasses: `color-11 box-10 p-20`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
