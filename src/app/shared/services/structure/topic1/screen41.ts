import { ScreenCustomExamComponent } from 'src/app/shared/components/screen-custom-exam/screen-custom-exam.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s41: StructureScreen = {
    title: 'AVALIAÇÃO',
    showInMenu: true,
    topicTitle: topicTitles[0],
    backgroundColor: null,
    backgroundImage: null,
    screenClasses: `padding`,
    items: [
        {
            type: ScreenImageComponent, classes: ``, styles: `position: absolute; top: 0; right: 0; bottom: 0;`,
            data: {
                source: `./assets/images/screen_1_59_1.jpg`, styles: `max-height: initial;`
            }
        },
        {
            type: ScreenImageComponent, classes: `col-3 offset-8`,
            styles: `margin-top: 360px;`,
            hideCurrentAndPreviousOnFinishCount: 4,
            data: {
                source: `./assets/images/screen_shared_start.png`,
                userShouldClick: true, hideCheck: true, hint: `Clique em Iniciar`
            }
        },
        {
            type: ScreenContentComponent, classes: `col-3 mb-20`,
            data: {
                content: `AVALIAÇÃO`,
                containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
        },
        {
            type: ScreenCustomExamComponent, classes: `col-12`,
            data: {
                questions: [
                    {
                        text: `De acordo com a tabela de Sistema de Gestão de Riscos da AGA, quais são as ferramentas básicas da Primeira Camada?`,
                        options: [
                            `GIM e PGM.`,

                            `PE E ART.`,

                            `WRAC e BOWTIE.`,

                            `PGM e ART.`
                        ],
                        correct: 2
                    },                    
                    {
                        text: `Qual ferramenta utilizamos para analisar e identificar os Riscos e Perigos no local de trabalho (<em>base line</em>), avaliar os riscos considerando os controles atuais e sua efetividade e priorizar os riscos para análise futura (separando os riscos mais críticos dos menos críticos)?`,
                        options: [
                            `LVCC.`,

                            `SICLOPE.`,

                            `WRAC.`,

                            `Abordagem.`
                        ],
                        correct: 2
                    },                    
                    {
                        text: `“Ter uma visão de toda a organização ou unidade operacional, identificar e analisar possíveis eventos indesejados, estabelecer controles importantes, documentar os requisitos e aplicar os resultados para reduzir o risco em questão a um nível tão baixo quanto razoavelmente praticável (ALARP)”.<br>O objetivo descrito acima pertence a qual das camadas do Sistema de Gestão de Riscos da AGA?`,
                        options: [
                            `Primeira Camada.`,

                            `Segunda Camada.`,

                            `Terceira Camada.`,

                            `Quarta Camada.`
                        ],
                        correct: 0
                    },                    
                    {
                        text: `É uma metodologia utilizada para avaliação, gerenciamento e o que é muito importante, comunicação de risco. Estamos referindo a:`,
                        options: [
                            `BOWTIE.`,

                            `Mapeamento de Processos.`,

                            `Procedimento Específico.`,

                            `GIM (Guia de Identificação da Mudança).`
                        ],
                        correct: 0
                    },                    
                    {
                        text: `São objetivos da segunda camada da Gestão de Riscos da AGA: identificar e avaliar os riscos relacionados a projetos, mudanças ou problemas; estabelecer e implantar os controles dos riscos identificados.<br>Quais são os Instrumentos de Gestão relacionados a essa camada?`,
                        options: [
                            `GIM e PGM.`,

                            `PE E ART.`,

                            `WRAC e BOWTIE.`,

                            `PGM e LVCC.`
                        ],
                        correct: 0
                    },                    
                    {
                        text: `O nosso sistema de gestão de risco é composto por 4 camadas. Na terceira camada quais são as ferramentas básicas?`,
                        options: [
                            `OPT (Observação Planejada da Tarefa).`,

                            `ART (Análise De Risco Da Tarefa - BOWTIE).`,

                            `Matriz de Risco - Gestão de Mudança.`,

                            `PE (Procedimento Específico) - ART (Análise de Risco da Tarefa).`
                        ],
                        correct: 3
                    },                    
                    {
                        text: `Qual é o objetivo da Terceira Camada?`,
                        options: [
                            `Aplicar técnicas de Avaliação de Riscos para identificar sistematicamente eventos indesejados relacionados à Segurança, à Saúde Ocupacional e ao Meio Ambiente.`,

                            `Identificar e avaliar os riscos relacionados a projetos, mudanças ou problemas. Estabelecer e implantar os controles dos riscos identificados.`,

                            `Garantir que os riscos do local de trabalho sejam avaliados e os controles e todos os recursos estejam disponíveis para realização da atividade.`,

                            `Definir o processo de gerenciamento de riscos de segurança, saúde e meio ambiente para atividades rotineiras e não rotineiras identificadas na avaliação de riscos de primeira camada da unidade.`
                        ],
                        correct: 3
                    },                    
                    {
                        text: `Quais são os três passos para a realização da Gestão da mudança?`,
                        options: [
                            `GIM (Guia de Identificação da Mudança) - ARM (Análise de Risco da Mudança) - PGM (Plano de Gerenciamento da Mudança).`,

                            `Analisar o evento - Considerar os controles - Avaliar a consequência.`,

                            `OPT - WRAC - LVCC. `,

                            `GIM (Guia de Identificação da Mudança - APR (Análise Preliminar de Risco) - Matriz de Risco.`
                        ],
                        correct: 0
                    },                    
                    {
                        text: `Quais são as fases que compõe a 2ª camada?`,
                        options: [
                            `Implementação da Mudança - Comunicação - Monitoramento - Gerenciamento de Documentos.`,

                            `Planejar - Organizar - Executar -Aplicar.`,

                            `Planejar - Fazer - Checar - Agir.`,

                            `Monitoramento e Gerenciamento. `
                        ],
                        correct: 0
                    },                    
                    {
                        text: `“É imprescindível a realização da mesma no local e horário onde será executada a atividade, pois os ambientes podem sofrer rápidas alterações, e tudo isso deve ser levado em consideração.” Estamos falando de qual ferramenta?`,
                        options: [
                            `WRAC.`,

                            `APR.`,

                            `LVCC.`,

                            `BOWTIE.`
                        ],
                        correct: 1
                    }                   
                ],
                feedbacks: [
                    {
                        text: `<div class="box-5 color-10">
                                <span class="wp-100">
                                    Você não atingiu o percentual de aproveitamento necessário para sua certificação.
                                </span>
                               </div>`,
                        containerClasses: ``,
                        minScore: 0, maxScore: 69, canTryAgain: true
                    },
                    {
                        text: `<div class="box-9 color-10">
                                <span class="wp-100">
                                    Parabéns!
                                    <br/><br/>
                                    Você concluiu a avaliação com sucesso.
                                </span>
                               </div>`,
                        containerClasses: ``,
                        minScore: 70, maxScore: 100, canTryAgain: false
                    }
                ],
                showAnswer: false,
                saveScore: true, passingScore: 70,
            }
        }
    ],
    completed: false
};
