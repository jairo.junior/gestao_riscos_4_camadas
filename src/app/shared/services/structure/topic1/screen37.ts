import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s37: StructureScreen = {
  title: 'GERENCIAMENTO DE RISCO DA 4ª CAMADA',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-20`,
            data: {
              content: `GERENCIAMENTO DE RISCO DA 4ª CAMADA`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-10 offset-1`,
            data: {
              charStyle: `full`,
              text: `Até aqui vimos as 3 primeiras camadas da Gestão de Riscos da AGA. Agora vamos iniciar a quarta e última Camada. Fique atento.`,
              audio: `./assets/audios/screen_1_54_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-8 offset-3`,
      styles: `position: absolute; top: 285px;`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-3`,
            data: {
              source: `./assets/images/screen_1_54_1.png`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-9 mt-30`,
            data: {
              content: 
              `<p>A quarta camada do gerenciamento de riscos garante que os riscos do local de trabalho sejam avaliados, e os controles e todos os recursos estejam disponíveis para realização da atividade.</p>
              
              <p class="mb-0">A Análise Preliminar de Riscos individual deve ser realizada no local de trabalho antes do início das atividades.</p>`,
              containerClasses: `color-1 box-3 p-20`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
