import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';


export const t1s7: StructureScreen = {
  title: 'MEDIDAS DE CONTROLE',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-4 mb-20`,
      data: {
        content: `MEDIDAS DE CONTROLE`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-6 offset-1`,
      styles: `margin-top: -47px;`,
      data: {
        charStyle: `partial`,
        text: `E tem mais! Faça o trabalho com segurança! Preste atenção nas mudanças e pense em todos esses passos.`,
        audio: `./assets/audios/screen_1_7_1.mp3`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-12`,
      data: {
        source: `./assets/images/screen_1_7_1.jpg`
      }
    }
  ],
  completed: false
};
