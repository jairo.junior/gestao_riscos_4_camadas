import { ScreenAlternatePopupComponent } from 'src/app/shared/components/screen-alternate-popup/screen-alternate-popup.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s26: StructureScreen = {
  title: '2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-30`,
      data: {
        content: `2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-20`,
      data: {
        content: `Clique nas imagens a seguir e conheça os documentos necessários para os registros da Segunda Camada.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12 bg-color-16 p-20 pdt-20`,
      styles: `min-height: 355px;`,
      data: {
        items: [
          {
            type: ScreenInnerContainerComponent, classes: `col-4`,
            data: {
              items: [
                {
                  type: ScreenContentComponent, classes: `col-12 offset-1 mb-10`,
                  data: {
                    content: `<strong>GIM</strong> (Guia de Identificação de Mudança)`,
                    containerClasses: `color-1 ft-11 ta-center`
                  }
                },
                {
                  type: ScreenImageComponent, classes: `col-12`,
                  data: {
                    source: `./assets/images/screen_1_34_1.png`,
                    styles: `min-height: unset;`,
                    userShouldClick: true,
                    hint: `Clique na imagem`,
                    imageLink: `./assets/attachments/screen_1_34_1.pdf`
                  }
                }
              ]
            }
          },
          {
            type: ScreenInnerContainerComponent, classes: `col-3`,
            data: {
              items: [
                {
                  type: ScreenContentComponent, classes: `col-12 offset-1 mb-10`,
                  data: {
                    content: `<strong>PGM</strong> (Plano de Gerenciamento de Mudança)`,
                    containerClasses: `color-1 ft-11 ta-center`
                  }
                },
                {
                  type: ScreenImageComponent, classes: `col-12`,
                  data: {
                    source: `./assets/images/screen_1_34_2.png`,
                    styles: `min-height: unset;`,
                    userShouldClick: true,
                    hint: `Clique na imagem`,
                    imageLink: `./assets/attachments/screen_1_34_2.pdf`
                  }
                }
              ]
            }
          },
          {
            type: ScreenInnerContainerComponent, classes: `col-5`,
            data: {
              items: [
                {
                  type: ScreenContentComponent, classes: `col-12 offset-1 mb-10`,
                  data: {
                    content: `Avaliação de Riscos de Mudança`,
                    containerClasses: `color-1 ft-11 ta-center`
                  }
                },
                {
                  type: ScreenImageComponent, classes: `col-12`,
                  data: {
                    source: `./assets/images/screen_1_34_3.png`,
                    styles: `min-height: unset;`,
                    userShouldClick: true,
                    hint: `Clique na imagem`,
                    imageLink: `./assets/attachments/screen_1_34_3.pdf`
                  }
                }
              ]
            }
          }

        ]
      }
    },
    {
      type: ScreenAlternatePopupComponent, classes: `col-12`,
      data: {
        buttons: [
          `<img src="./assets/images/screen_shared_know_more.png"/>`,
        ],
        buttonsStyles: [
          `position: absolute;
          bottom: 55px;
          right: 150px;`
        ],
        checkStyles: [`left: 85px;`],
        customHint: `Clique em Saiba Mais`,
        contentBoxStyles: `width: 500px;`,
        contentBoxClasses: `box-3 color-5`,
        contents:
          [`<div class="container-fluid">
            <div class="row">
                <div class="col-12">
                  <p><strong>Quer saber um pouco mais sobre a 2ª camada?</strong></p>
                  <p>Consulte o treinamento disponível sobre Gestão da Mudança na Trilha do Conhecimento.</p>
                  <p class="mb-0"><strong>Não deixe de conferir!</strong></p>
                </div>
            </div>
        </div>`]
      }
    }
  ],
  completed: false
};
