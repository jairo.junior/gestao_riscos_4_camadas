import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s34: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-10`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-11 pdl-20`,
            data: {
              charStyle: `full`,
              text: `A próxima etapa é a criação do procedimento específico. Esse procedimento deve ser realizado antes de se iniciar novas atividades que contenham algum fator de risco classificado como ”risco moderado ou maior.”`,
              audio: `./assets/audios/screen_1_49_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenContentComponent, classes: `col-9 offset-2 pdl-20 pdr-20`,
      styles: `position: absolute; top: 277px;`,
      data: {
        content: `ETAPA: Criação do Procedimento Específico`,
        containerClasses: `ft-opensans-bold color-11`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-9 offset-2 pdl-20 pdr-20`,
      styles: `position: absolute; top: 310px;`,
      data: {
        content: 
        `<div class="ft-14 bg-color-1 color-3 ta-center" style="padding: 5px; margin-bottom: 4px;">
          <strong>A criação do Procedimento Específico (PE) deve considerar:</strong>
        </div>

        <div class="bg-color-2 ft-14 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-Os aspectos ambientais da tarefa a ser executada.</span>
        </div>
        
        <div class="bg-color-3 ft-14 color-1" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-Os eventos indesejados prioritários identificados nas avaliações de risco de primeira e segunda camadas.</span>
        </div>
        
        <div class="bg-color-2 ft-14 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-As tarefas rotineiras e não rotineiras associadas com os Eventos Indesejados Prioritários.</span>
        </div>
        
        <div class="bg-color-3 ft-14 color-1" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-As opções específicas disponíveis para controlar eventuais exposições perigosas na atividade.</span>
        </div>
        
        <div class="bg-color-2 ft-14 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-Eventuais controles críticos já identificados.</span>
        </div>
        
        <div class="bg-color-3 ft-14 color-1" style="padding: 4px 10px; margin-bottom: 4px;">
          <span>-A revisão dos controles identificados na ART.</span>
        </div>`,
      }
    }
  ],
  completed: false
};
