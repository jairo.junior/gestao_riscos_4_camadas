import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s22: StructureScreen = {
  title: '2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-8`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-8 offset-1`,
            data: {
              charStyle: `full`,
              text: `Chegamos à Segunda Camada. Preste atenção!`,
              audio: `./assets/audios/screen_1_30_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-4`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-12`,
            data: {
              source: `./assets/images/screen_1_30_1.png`,
              styles: `max-height: unset;`
            }
          }          
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-9 offset-2 pdl-40`,
      styles: `position: absolute; top: 247px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `Objetivo e Aplicação da Segunda Camada`,
              containerClasses: `ft-opensans-bold color-11`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-6 pdr-10`,
            data: {
              content: 
              `<div class="row">
                <div class="col-12" style="min-height: 208px;">
                  <p class="mb-0"><strong>OBJETIVO:</strong><br>Identificar e avaliar os riscos relacionados a projetos, mudanças ou problemas. Estabelecer e implantar os controles dos riscos identificados.<br>Ex.: <strong>GIM</strong> (Guia de Identificação de Mudança), <strong>PGM</strong> (Plano de Gerenciamento de Mudança) e Análise de Riscos de Mudança.</p>
                </div>
              </div>`,
              containerClasses: `ft-14 box-16 color-5 p-20`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-6 pdl-10`,
            data: {
              content: 
              `<div class="row d-flex align-items-center mb-10">
                <div class="col-2">
                  <img src="./assets/images/screen_1_30_2.png" />
                </div>
                <div class="col-10">
                  <p class="mb-0"><strong>APLICAÇÃO: </strong>deve ser aplicado em mudanças relacionadas a equipamentos, sistemas, ambiente de trabalho, projeto e processos industriais.</p>
                </div>
              </div>
              
              <div class="row d-flex align-items-center">
                <div class="col-2">
                  <img src="./assets/images/screen_1_30_3.png" />
                </div>
                <div class="col-10">
                  <p class="mb-0">Se houver procedimento que permita variações, desde que especificadas, e anteriormente analisados seus riscos, <strong>não é uma mudança</strong>.</p>
                </div>
              </div>`,
              containerClasses: `ft-14 box-4 color-14 p-20`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
