import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';


export const t1s13: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-7 mb-20`,
      data: {
        content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 mb-20`,
      data: {
        content: `Mas para que servem especificamente os documentos WRAC e Bowtie? Fique por dentro!`,
        containerClasses: `color-11 pdl-20 pdr-20`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-5 offset-1`,
      data: {
        source: `./assets/images/screen_1_16_1.jpg`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-5`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `Metodologia para mapeamento dos fatores de riscos, avaliação, classificação e priorização dos riscos e controles.`,
              containerClasses: `ft-14 color-10 bg-color-2 p-10`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-30`,
            data: {
              content: `Metodologia que permite identificar os riscos críticos e definir os controles críticos.`,
              containerClasses: `ft-14 color-10 bg-color-8 p-10`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `Lista de Verificação de Controles Críticos (LVCC). Acesse o documento em PDF.`,
              containerClasses: `ft-14 color-1 bg-color-4 p-10`
            }
          }
        ]
      }
    }

  ],
  completed: false
};
