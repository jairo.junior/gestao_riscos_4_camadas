import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s17: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-7`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-12`,
            data: {
              charStyle: `full`,
              text: `Para a definição das recomendações de novos controles, a equipe deve considerar a hierarquia: Eliminação, Substituição, Engenharia, Separação, Administrativo, EPI e, ainda, o modelo SMART (Específico, Mensurável, com o Responsável, Razoável, Tempestivo).`,
              audio: `./assets/audios/screen_1_20_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-5 mt-50`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `AVALIAÇÃO DE RISCOS`,
              containerClasses: `ft-opensans-bold color-11 ta-center`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-12 mb-10`,
            data: {
              source: `./assets/images/screen_1_20_1.png`,
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 pdl-10 pdr-10`,
            data: {
              content: `Para cada controle novo proposto deve haver um plano de ação para sua implementação com definição do prazo e do responsável. A evolução deste plano de ação deve ser monitorada.`,
              containerClasses: `color-10 bg-color-2 p-20 ft-14`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-7`,
      styles: `position: absolute; top: 335px; left: 0; padding-left: 120px;`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-12 mb-10`,
            data: {
              source: `./assets/images/screen_1_20_2.png`,
            }
          },
          {
            type: ScreenImageComponent, classes: `col-1`,
            styles: `position: absolute; top: 170px; right: 10px;`,
            data: {
              userShouldClick: true,
              hint: `Clique na lupa`,
              source: `./assets/images/ico-forget.png`,
              imageLink: `./assets/attachments/screen_1_20_1.pdf`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
