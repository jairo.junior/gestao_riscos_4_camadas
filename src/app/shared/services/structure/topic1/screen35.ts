import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s35: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-10`,
      data: {
        content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 pdl-30 pdr-30 mb-10`,
      data: {
        content: `<p class="mb-10">Depois de elaborado o procedimento específico, deve-se assegurar de que as pessoas que irão executar a atividade possuem as competências corretas para que ela seja executada com sucesso. Estamos falando da etapa de Capacitação.</p>

        <p class="mb-10"><strong>ETAPA: Capacitação</strong></p>
        
        <p class="mb-0">O Supervisor da equipe deve estar capacitado para assegurar a implementação do procedimento, e um módulo de treinamento deve ser aplicado e adequado para o público-alvo.</p>`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-12`,
      data: {
        source: `./assets/images/screen_1_52_1.png`,
      }
    }
  ],
  completed: false
};
