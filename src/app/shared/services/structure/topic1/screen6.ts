import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';


export const t1s6: StructureScreen = {
  title: 'MEDIDAS DE CONTROLE',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-4 mb-10`,
      data: {
        content: `MEDIDAS DE CONTROLE`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 mb-10`,
      data: {
        content: `Além das três medidas de controles mencionadas anteriormente, é imprescindível usar todos os EPIs obrigatórios para a atividade.`,
        containerClasses: `color-11 ta-center`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-6`,
      data: {
        source: `./assets/images/screen_1_6_1.jpg`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-6`,
      data: {
        source: `./assets/images/screen_1_6_2.jpg`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mt-10`,
      data: {
        content: 
        `<div class="row d-flex align-items-center">
          <div class="col-3 ta-right">
            <img src="./assets/images/screen_shared_warn.png" />
          </div>
          <div class="col-9 bg-color-3 p-10" style="margin-left: -1px;">
            <p class="mb-0 color-1">Em caso de dúvida, converse com o supervisor antes de iniciar uma atividade.</p>
          </div>
         </div>`
      }
    }
  ],
  completed: false
};
