import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s23: StructureScreen = {
  title: '2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-20`,
      data: {
        content: `2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-20`,
      data: {
        content: `Nessa fase é realizada a Análise de Risco de Projeto, Mudança ou Problema. Veja a lista de atividades.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12 bg-color-12 pdt-30 pdb-40`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-6 offset-1`,
            data: {
              content:
                `<div class="bg-color-1 color-3 ta-center" style="padding: 5px; margin-bottom: 4px;">
                <strong>Análise de Riscos de Projeto, Mudança ou Problema</strong>
              </div>
      
              <div class="bg-color-9 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Guia de Identificação de Mudança (GIM)</span>
              </div>
              
              <div class="bg-color-12 color-8" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Análise de Riscos da Mudança</span>
              </div>
      
              <div class="bg-color-2 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Plano de Gerenciamento da Mudança</span>
              </div>
      
              <div class="bg-color-12 color-8" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Aprovações conforme o risco</span>
              </div>
      
              <div class="bg-color-7 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Comunicação para partes interessadas</span>
              </div>
      
              <div class="bg-color-12 color-8" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Monitoramento da Mudança</span>
              </div>
      
              <div class="bg-color-8 color-10" style="padding: 4px 10px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Indicador (GMp e GMr)</span>
              </div>`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-4`,
            data: {
              source: `./assets/images/screen_1_31_1.png`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
