import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s16: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `<strong>AVALIAÇÃO DE RISCOS</strong><br>A primeira ferramenta utilizada nesta fase deve ser a WRAC (Workplace Risk Assessment and Control), ou Avaliação de Riscos e Controles do Local de Trabalho.`,
              containerClasses: `color-11 pdl-20 pdr-20`
            }
          }
        ]
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 p-20 box-16`,
      styles: `height: 315px;`,
      data: {
        content:
          `<div class="row">
              <div class="col-10 offset-1">
                <p>A WRAC deve ser realizada para cada etapa das atividades identificadas no mapeamento de processos, de acordo com a seguinte ordem:</p>
                <p class="mb-0 ta-center"><img src="./assets/images/screen_1_19_1.png" /></p>
              </div>
           </div>`
      }
    }
  ],
  completed: false
};
