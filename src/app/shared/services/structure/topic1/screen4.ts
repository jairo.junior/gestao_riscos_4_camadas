import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s4: StructureScreen = {
  title: 'VISÃO GERAL',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-3 mb-40`,
            data: {
              content: `VISÃO GERAL`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-5`,
            data: {
              content: `No modelo de camadas, a avaliação de riscos é dividida em quatro etapas. Acesse a tabela a seguir e veja todo o processo de modo geral.`,
              containerClasses: `color-11 p-40 mt-40`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-7`,
            data: {
              source: `./assets/images/screen_1_4_1.png`,
              styles: `max-height: unset;`
            }
          }
        ]
      }
    },
    {
      type: ScreenImageComponent, classes: `col-3`,
      styles: `position: absolute; top: 380px; left: 80px;`,
      data: {
        source: `./assets/images/screen_shared_increase.png`,
        userShouldClick: true,
        hint: `Clique no botão Anexo`,
        checkStyles: [`right: -10px; top: 27px;`],
        imageLink: `./assets/attachments/screen_1_4_1.pdf`
      }
    }
  ],
  completed: false
};
