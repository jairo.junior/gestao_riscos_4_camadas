import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s31: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-20`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-10 offset-1`,
            data: {
              charStyle: `full`,
              text: `Você viu que é um direito seu recusar-se a realizar um trabalho caso se depare com um fator de risco. Entenda melhor esse direito.`,
              audio: `./assets/audios/screen_1_40_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-10 offset-2 pdl-60 pdr-40`,
      styles: `position: absolute; top: 270px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-10`,
            data: {
              content: `Direito de Recusa`,
              containerClasses: `ft-opensans-bold color-11`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-6 pdr-10`,
            data: {
              content: 
              `<div class="row">
                <div class="col-12">
                  <p class="mb-0 ta-center"><img src="./assets/images/screen_1_40_1.png" /></p>
                  <p class="mb-0">É um direito do empregado próprio/contratado se recusar a executar tarefas sob condição não conforme, devendo comunicar à sua supervisão ou gerência, para que medidas de prevenção sejam tomadas para eliminar um fator de risco ou reduzir seu risco a nível aceitável.</p>
                </div>
              </div>`,
              containerClasses: `ft-14 box-3 color-5 p-20`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-6 pdl-10`,
            data: {
              content: 
              `<div class="row d-flex align-items-center" style="min-height: 208px;">
                <div class="col-12">
                  <p>O Supervisor ou Líder imediato deverá revisar a respectiva Análise de Riscos da Tarefa (ART).</p>
                  
                  <p class="mb-0">O exercício do Direito de Recusa não implicará nenhuma penalização para o empregado próprio/contratado.</p>
                </div>
              </div>`,
              containerClasses: `ft-14 box-4 color-14 p-20`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
