import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s25: StructureScreen = {
  title: '2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-20`,
      data: {
        content: `2ª CAMADA – AVALIAÇÃO DE RISCO E MUDANÇA`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-50`,
      data: {
        content: `A Segunda Camada é composta por quatro fases, como vemos na sequência.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-12`,
      data: {
        source: `./assets/images/screen_1_33_1.png`
      }
    }
  ],
  completed: false
};
