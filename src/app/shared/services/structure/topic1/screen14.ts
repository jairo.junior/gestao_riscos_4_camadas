import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s14: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `<strong>INVENTÁRIO DE FATORES DE RISCOS/ASPECTOS AMBIENTAIS</strong>`,
              containerClasses: `color-11 pdl-20`
            }
          }
        ]
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-6 pdl-10 pdr-10 mb-20`,
      // styles: `margin-top: -47px;`,
      data: {
        charStyle: `partial`,
        text: `Concluído o mapeamento dos processos, os Gerentes Sêniores, Gerentes de Área e Supervisores, envolvendo suas equipes, realizam o inventário de fatores de riscos/aspectos ambientais associados aos processos mapeados.`,
        audio: `./assets/audios/screen_1_17_1.mp3`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 pdl-40 pdr-40`,
      data: {
        content: 
        `<div class="row">
          <div class="col m-10 box-3">
            <p class="mb-0 ft-14 ta-center color-5">O fator de risco ou aspecto ambiental tem de ser reconhecido e entendido para que seja possível gerenciar o risco ou impacto ambiental associado.</p>
          </div>
          
          <div class="col m-10 box-2">
            <p class="mb-0 ft-14 ta-center color-10">Nesta etapa, todas as energias/fontes de dano devem ser identificadas para cada atividade mapeada no processo.</p>
          </div>

          <div class="col m-10 box-9">
            <p class="mb-0 ft-14 ta-center color-10">A magnitude da energia/fonte de dano e os mecanismos de liberação devem ser claramente definidos de acordo com a matriz de avaliação de riscos da AGA.</p>
          </div>
        </div>`
      }
    }
  ],
  completed: false
};
