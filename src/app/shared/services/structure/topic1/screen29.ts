import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s29: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-8`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-11 offset-1`,
            data: {
              charStyle: `full`,
              text: `Na 3ª camada, a Análise de Risco de Tarefa Rotineira e Não Rotineira é realizada com o apoio de dois instrumentos de gestão. Veja quais são eles.`,
              audio: `./assets/audios/screen_1_38_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-10 offset-2 pdl-40 pdr-40`,
      styles: `position: absolute; top: 290px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mt-60`,
            data: {
              content: 
              `<div class="bg-color-1 color-3 ta-center" style="padding: 5px; margin-bottom: 4px;">
                <strong>Análise de Tarefa Rotineira e Não Rotineira</strong>
              </div>
      
              <div class="bg-color-9 color-10" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">PE – Procedimento Específico baseado em Risco</span>
              </div>
              
              <div class="bg-color-12 color-8" style="padding: 4px 10px; margin-bottom: 4px;">
                <img src="./assets/images/screen_1_13_1.png" />
                <span class="pdl-10">Análise de Risco da Tarefa – ART</span>
              </div>`,
            }
          },
          {
            type: ScreenImageComponent, classes: `col-4`,
            data: {
              source: `./assets/images/screen_1_31_1.png`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
