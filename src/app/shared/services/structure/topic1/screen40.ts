import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s40: StructureScreen = {
  title: 'ENCERRAMENTO',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-6 mb-40`,
            data: {
              content: `ENCERRAMENTO`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-12`,
            data: {
              charStyle: `full`,
              text: `Muito bem! Chegamos ao final do nosso curso sobre as 4 camadas da Gestão de Riscos. Esperamos que você tenha absorvido bem os conhecimentos construídos aqui e os coloque em prática sempre que necessário. Até a próxima!`,
              audio: `./assets/audios/screen_1_58_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12`,
            data: {
              content: `Avance de tela para acessar a Avaliação Final!`,
              containerClasses: `color-8 ft-25 ft-opensans-bold ta-center p-20`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-12`,
            data: {
              source: `./assets/images/screen_1_58_1.jpg`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
