import { ScreenAlternatePopupComponent } from 'src/app/shared/components/screen-alternate-popup/screen-alternate-popup.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s33: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_1_42_1.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-8 mb-50`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12 mb-20`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-4 offset-1`,
            data: {
              content: `É importante que as tarefas associadas com Eventos Indesejados Prioritários identificados por meio das avaliações de riscos de 1ª e 2ª camadas sejam claramente entendidos por todos os envolvidos na atividade.`,
              containerClasses: `box-3 color-5`
            }
          }
        ]
      }
    },
    {
      type: ScreenAlternatePopupComponent, classes: `col-2 offset-3`,
      data: {
        buttons: [
          `<img src="./assets/images/screen_shared_know_more.png"/>`,
        ],
        buttonsStyles: [`margin-left: auto;`],
        customHint: `Clique em Saiba Mais`,
        contentBoxStyles: `width: 815px;`,
        contentBoxClasses: `box-4`,
        contents:
          [
            `<div class="container-fluid">
              <div class="row">
                  <div class="col-12">
                    <p class="color-1"><strong>Procedimentos de Segurança Operacional/Análises de Risco da Tarefa (ART) para as atividades</strong></p>
                    <p class="mb-0 ta-center"><img src="./assets/images/screen_1_42_2.png"/></p>
                  </div>
              </div>
          </div>`,
          ]
      }
    }
  ],
  completed: false
};
