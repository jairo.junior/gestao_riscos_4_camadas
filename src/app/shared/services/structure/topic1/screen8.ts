import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenAlternatePopupComponent } from 'src/app/shared/components/screen-alternate-popup/screen-alternate-popup.component';


export const t1s8: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-7`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-11 offset-1`,
            data: {
              charStyle: `full`,
              text: `A partir de agora vamos aprender sobre cada uma das quatro camadas que compõem a gestão de riscos da AGA. Obviamente, vamos começar com a Primeira Camada. Siga em frente!`,
              audio: `./assets/audios/screen_1_8_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-5`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-12`,
            data: {
              source: `./assets/images/screen_1_8_1.png`,
              styles: `max-height: unset;`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 p-10`,
            data: {
              content: `Esta camada deve abranger Riscos e Perigos considerados maiores, dos processos produtivos.`,
              containerClasses: `color-1 bg-color-3 p-20`
            }
          }
        ]
      }
    },
  ],
  completed: false
};
