import { ScreenAlternateComponent } from 'src/app/shared/components/screen-alternate/screen-alternate.component';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s11: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-7 mb-20`,
      data: {
        content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-9 offset-1`,
      data: {
        charStyle: `full`,
        text: `Mas quais são as ações que devemos executar nessa etapa? Navegue pela tela e descubra.`,
        audio: `./assets/audios/screen_1_12_1.mp3`
      }
    },
    {
      type: ScreenAlternateComponent, classes: `col-7 offset-3`,
      styles: `position: absolute; top: 250px;`,
      data: {
        buttons: [
          `<img src="./assets/images/screen_1_12_1.png" />`,
          `<img src="./assets/images/screen_1_12_2.png" />`,
          `<img src="./assets/images/screen_1_12_3.png" />`,
          `<img src="./assets/images/screen_1_12_4.png" />`
        ],
        checkStyles: [
          `right: 440px;`,
          `right: 290px;`,
          `right: 145px;`,
          `right: 0;`
        ],
        preContent: 
        `<div class="row box-3 p-20" style="min-height: 216px; margin-top: -1px;">
            <div class="col-12">
              <p class="mb-0 ta-center ft-18"><strong>GUIA DE AVALIAÇÃO DE RISCOS</strong></p>
            </div>
        </div>`,
        contentBoxClasses: `ft-14`,
        contents: [
          `<div class="row box-2 p-20" style="min-height: 216px; margin-top: -1px;">
             <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_5.png" />
             </div>
             <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-2 pdt-10 pdb-10 pdl-20 pdr-20">Desenvolver Mapeamento de Processo.</p>
             </div>
             <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_6.png" />
             </div>
             <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-2 pdt-10 pdb-10 pdl-20 pdr-20">Desenvolver um Inventário de Perigos para cada passo no Mapeamento de Processo.</p>
             </div>
           </div>`,

          `<div class="row box-7 p-20" style="min-height: 216px; margin-top: -1px;">
            <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_7.png" />
            </div>
            <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-7 pdt-10 pdb-10 pdl-20 pdr-20">Fazer Avaliação de Risco para cada passo no Mapeamento e Processo com base no Inventário de Perigos.</p>
            </div>
            <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_8.png" />
            </div>
            <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-7 pdt-10 pdb-10 pdl-20 pdr-20">Análise de Risco baseada na Matriz de Riscos do Grupo AGA.</p>
            </div>
            <div class="col-2 ta-center">
              <img src="./assets/images/screen_1_12_9.png" />
            </div>
            <div class="col-10">
              <p class="mb-0 ml-10 box-16 color-7 pdt-10 pdb-10 pdl-20 pdr-20">Identificação dos Eventos Indesejados Prioritários.</p>
            </div>
           </div>`,
           
          `<div class="row box-8 p-20" style="min-height: 216px; margin-top: -1px;">
            <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_10.png" />
            </div>
            <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-8 pdt-10 pdb-10 pdl-20 pdr-20">Fazer uma Análise Bowtie para cada evento prioritário.</p>
            </div>
            <div class="col-2 ta-center mb-10">
              <img src="./assets/images/screen_1_12_11.png" />
            </div>
            <div class="col-10 mb-10">
              <p class="mb-0 ml-10 box-16 color-8 pdt-10 pdb-10 pdl-20 pdr-20">Criar Registro de Riscos e Controles Críticos (atual e adicional), Responsabilidade e Accountability.</p>
            </div>
            <div class="col-2 ta-center">
              <img src="./assets/images/screen_1_12_12.png" />
            </div>
            <div class="col-10">
              <p class="mb-0 ml-10 box-16 color-8 pdt-10 pdb-10 pdl-20 pdr-20">Desenvolver Planos de Resposta com Gatilho de Ações.</p>
            </div>
           </div>`,

          `<div class="row box-9 p-20" style="min-height: 216px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_12_13.png" />
            </div>
            <div class="col-9 mb-10">
              <ul class="ml-10 bg-color-16 color-9 pdt-10 pdb-10">
                <li>Avaliação de Riscos <em>Baseline</em>.</li>
                <li>Registro de Riscos e Controles Críticos.</li>
                <li>Resultado retroalimenta a Estratégia.</li>
                <li>Objetivos & Metas e Gestão do BPF.</li>
              </ul>
            </div>
           </div>`
        ]
      }
    }
  ],
  completed: false
};
