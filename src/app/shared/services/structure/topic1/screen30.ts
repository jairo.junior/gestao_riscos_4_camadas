import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s30: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-20`,
      data: {
        content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-10`,
      data: {
        content: `Explore a tela e veja o fluxograma de atividades desta fase. Além disso, disponibilizamos para você um modelo da Análise de Riscos da Tarefa – ART.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-8 offset-1`,
      data: {
        source: `./assets/images/screen_1_39_1.png`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-2`,
      data: {
        source: `./assets/images/screen_shared_know_more.png`,
        checkStyles: `right: 20px; top: -9px;`,
        userShouldClick: true,
        hint: `Clique em Saiba Mais`,
        imageLink: `./assets/attachments/screen_1_39_1.pdf`
      }
    }
  ],
  completed: false
};
