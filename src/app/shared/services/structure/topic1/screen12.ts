import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s12: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `<strong>MAPEAMENTO DE PROCESSOS</strong><br>Essa etapa deve ter a participação de Diretores, Gerentes Sêniores, Gerentes de Área e Supervisores.`,
              containerClasses: `color-11 pdl-20`
            }
          }
        ]
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-6 pdl-20 pdr-20`,
      styles: `margin-top: -47px;`,
      data: {
        charStyle: `partial`,
        text: `Para a elaboração do mapeamento de processos de uma unidade, o processo fundamental deve ser claramente definido, contemplando os subprocessos e as atividades rotineiras e não rotineiras.`,
        audio: `./assets/audios/screen_1_14_1.mp3`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-6 offset-1`, 
      data: {
        source: `./assets/images/screen_1_14_1.jpg`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-3`, 
      data: {
        userShouldClick: true,
        hint: `Clique no botão Anexo`,
        source: `./assets/images/screen_shared_increase.png`,
        imageLink: `./assets/attachments/screen_1_14_1.pdf`
      }
    }
  ],
  completed: false
};
