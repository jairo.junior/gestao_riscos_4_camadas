import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s32: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-8 mb-20`,
      data: {
        content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-20`,
      data: {
        content: `Essa fase possui uma série de passos que orientam o empregado durante uma tarefa, do começo ao fim, em uma ordem cronológica. É o que chamamos de Procedimentos Específicos.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-10 offset-1`,
      data: {
        source: `./assets/images/screen_1_41_1.png`
      }
    }
  ],
  completed: false
};
