import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s20: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          }
        ]
      }
    },
    {
      type: ScreenContentComponent, classes: `col-4 pdl-20 mb-20`,
      data: {
        content: `<strong>ANÁLISE DE RISCOS BOWTIE (BTA)</strong>`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-4 pdl-40 mt-70`,
            data: {
              content: `A Análise de Riscos Bowtie (BTA) deve ser realizada com a participação dos Gerentes Sêniores, Gerentes de Área e Supervisores, com suporte de um especialista em risco, área de SSO e de Meio Ambiente.`,
              containerClasses: `color-11`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-7`,
            data: {
              source: `./assets/images/screen_1_24_1.png`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
