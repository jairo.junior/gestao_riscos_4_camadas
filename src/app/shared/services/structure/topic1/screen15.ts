import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s15: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `<strong>AVALIAÇÃO DE RISCOS</strong>`,
              containerClasses: `color-11 pdl-20`
            }
          }
        ]
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-6 pdl-10 pdr-10`,
      styles: `margin-top: -47px;`,
      data: {
        charStyle: `partial`,
        text: `Assim como o inventário de fatores de risco, a etapa de avaliação de riscos também deve ser realizada com a participação dos Gerentes Sêniores, Gerentes de Área e Supervisores, envolvendo suas equipes.`,
        audio: `./assets/audios/screen_1_18_1.mp3`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-12 p-20 bg-color-7`,
      styles: `height: 297px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 pdl-40 pdr-40 mb-20`,
            data: {
              content: `A primeira avaliação deve ser feita sem considerar os controles existentes, para a identificação do risco puro.`,
              containerClasses: `ft-14 box-3 color-1 p-10`
            }
          },
          {
            type: ScreenInnerContainerComponent, classes: `col-12 pdl-40 pdr-40`,
            data: {
              items: [
                {
                  type: ScreenImageComponent, classes: `col-5`,
                  data: {
                    source: `./assets/images/screen_1_18_1.png`
                  }
                },
                {
                  type: ScreenContentComponent, classes: `col-7 pdl-30`,
                  data: {
                    content: 
                    `<p>Deve-se levar em consideração:</p>
                      <ul>
                        <li>os incidentes e quase-incidentes de SSO e ambientais para a identificação dos eventos indesejados potenciais.</li>
                        <li>os controles atuais, possíveis melhorias e projetar controles adicionais considerando o SMART (Específico, Mensurável, Alcançável, Relevante e Temporal).</li>
                      </ul>`,
                    containerClasses: `color-10`
                  }
                }
              ]
            }
          }
        ]
      }
    }
  ],
  completed: false
};
