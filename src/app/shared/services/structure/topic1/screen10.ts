import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';


export const t1s10: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-7 mb-20`,
      data: {
        content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1 mb-50`,
      data: {
        content: `A avaliação de riscos de primeira camada é desenvolvida através da Técnica de Avaliação de Risco do Ambiente de Trabalho, ou WRAC (<em>Workplace Risk Assessment and Control</em>).`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-4 offset-1`,
      data: {
        source: `./assets/images/screen_1_11_1.jpg`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-6 pdt-40 pdl-30`,
      data: {
        content: 
        `<strong>WRAC - Workplace Risk Assessment and Control</strong><br><br>
        
        Técnica de avaliação de Riscos e controle do Ambiente de Trabalho, estruturada para avaliar, escalonar e controlar os Riscos e Perigos dos locais de trabalho.`,
        containerClasses: `box-3 color-1`
      }
    },
  ],
  completed: false
};
