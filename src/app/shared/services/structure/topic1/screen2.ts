import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';


export const t1s2: StructureScreen = {
  title: 'INTRODUÇÃO',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-3 mb-30`,
      data: {
        content: `INTRODUÇÃO`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-10 offset-1`,
      data: {
        charStyle: `full`,
        text: `Olá! Como vai? Legal ver você por aqui novamente! Hoje nosso assunto é a Gestão de Risco da AngloGold Ashanti e suas quatro camadas. Vamos lá?`,
        audio: `./assets/audios/screen_1_2_1.mp3`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-8 offset-3`,
      styles: `position: absolute; top: 260px;`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-5 pdt-30`,
            data: {
              source: `./assets/images/screen_1_2_1.png`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-7 pdr-10`,
            data: {
              content: 
              `<p><strong>Objetivo:</strong></p>
              
              <p class="mb-0">Conhecer o processo de gerenciamento de riscos e perigos de Segurança, Saúde e Meio Ambiente que deverá ser aplicado em todas as operações da AngloGold Ashanti.</p>`,
              containerClasses: `box-3 color-1`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
