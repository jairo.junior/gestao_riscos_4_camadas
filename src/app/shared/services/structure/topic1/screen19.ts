import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s19: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          }
        ]
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-5`,
      data: {
        charStyle: `full`,
        text: `Sobre a metodologia Bowtie podemos afirmar que sua força está na simplicidade. Menos é mais. Vamos ver algumas definições sobre essa metodologia.`,
        audio: `./assets/audios/screen_1_22_1.mp3`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-7 pdl-30 pdr-30`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-10`,
            data: {
              content: `ANÁLISE DE RISCOS BOWTIE (BTA)`,
              containerClasses: `ft-opensans-bold color-11 ta-center`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-12 mb-10`,
            data: {
              source: `./assets/images/screen_1_22_1.png`,
            }
          }
        ]
      }
    }
  ],
  completed: false
};
