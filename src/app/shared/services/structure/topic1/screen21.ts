import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s21: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          }
        ]
      }
    },
    {
      type: ScreenContentComponent, classes: `col-11 pdl-20 mb-20`,
      data: {
        content: `<strong>ANÁLISE DE RISCOS BOWTIE (BTA)</strong><br>Para fins da BTA, uma ameaça é o mecanismo potencial que pode liberar o fator de riso, causando assim o evento indesejado prioritário.`,
        containerClasses: `color-11`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-10 offset-1`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-4`,
            data: {
              content: `<strong>Identificar Causas Potenciais</strong><br>Graficamente, esses elementos são mostrados no lado esquerdo do evento indesejado prioritário, conforme imagem ao lado. Para documentação, eles deverão ser listados na coluna Causa do formulário de BTA.`,
              containerClasses: `color-1 box-3 p-20`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-8`,
            data: {
              source: `./assets/images/screen_1_25_1.png`,
              styles: `margin-left: -1px;`
            }
          }
        ]
      }
    },
    {
      type: ScreenContentComponent, classes: `col-10 offset-1`,
      data: {
        content: `Imagem da análise Bowtie.`,
        containerClasses: `color-11 ta-right ft-14`
      }
    }
  ],
  completed: false
};
