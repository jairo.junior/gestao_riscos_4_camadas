import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s18: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_1_21_1.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-10 offset-1`,
            data: {
              content: 
              `<p>Ainda falta considerarmos a Identificação dos Eventos Indesejados Prioritários.</p>

               <p>Esta etapa deve ser realizada com a participação dos Gerentes Sêniores, Gerentes de Área e Supervisores, com suporte de um especialista em risco e da área de SSO e de Meio Ambiente.</p>

               <p class="mb-0">Os riscos classificados como “Maior” ou “Extremo” na Matriz de Avaliação de Riscos devem ser considerados como Prioritários e uma análise de risco Bowtie deve ser realizada para a identificação dos controles críticos para a prevenção do evento indesejado prioritário associado.</p>`,
              containerClasses: `color-10`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-30`,
            data: {
              content: `ANÁLISE DE RISCOS BOWTIE (BTA)`,
              containerClasses: `ft-opensans-bold color-11 ta-center`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-12 mb-30`,
            data: {
              source: `./assets/images/screen_1_21_1.png`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-10 offset-1`,
            data: {
              content: `O Gerente da área deverá verificar se as classificações dos eventos indesejados foram realistas e se são consistentes com a experiência da empresa e da indústria.`,
              containerClasses: `box-3 color-1`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
