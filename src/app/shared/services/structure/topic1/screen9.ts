import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s9: StructureScreen = {
  title: '1ª CAMADA – GERENCIAMENTO DE RISCOS',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: null,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-7 mb-20`,
      data: {
        content: `1ª CAMADA – GERENCIAMENTO DE RISCOS`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 mb-20`,
      data: {
        content: `Um resultado requerido dessa camada é o desenvolvimento e uso contínuo de um Registro de Riscos e Controles Críticos. Navegue pela tela e conheça outros objetivos desta etapa.`,
        containerClasses: `color-11 pdl-20 pdr-20`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 p-30`,
      styles: `background-color: #FFF4C5;`,
      data: {
        backButton: `./assets/images/screen_shared_pop_arrow_left.png`,
        nextButton: `./assets/images/screen_shared_pop_arrow_right.png`,
        content:
          `<div class="row ml-40 mr-40 d-flex align-items-center">
              <div class="col-7 color-1">
                <p><strong>Objetivo da Primeira Camada</strong></p>

                <p>Ter uma visão de toda a organização ou unidade operacional, identificar e analisar possíveis eventos indesejados, estabelecer controles importantes, documentar os requisitos e aplicar os resultados para reduzir o risco em questão a um nível tão baixo quanto razoavelmente praticável (ALARP).</p>
               
                <p class="mb-0">Além disso, produzir e manter um perfil priorizado dos possíveis eventos indesejados da Produção, os controles críticos e as responsabilidades relevantes.</p>
              </div>
              
              <div class="col-5 ta-center">
                <img src="./assets/images/screen_1_9_1.png">
              </div>
          </div>`,
        containerClasses: `color-11`
      }
    },
  ],
  completed: false
};
