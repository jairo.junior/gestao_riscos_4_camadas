import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenCustomQuestionTrueOrFalseComponent } from 'src/app/shared/components/screen-custom-question-true-or-false/screen-custom-question-true-or-false.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s27: StructureScreen = {
    title: 'ATIVIDADE INTERATIVA – 1',
    showInMenu: true,
    topicTitle: topicTitles[0],
    backgroundColor: null,
    backgroundImage: null,
    screenClasses: `padding`,
    items: [
        {
            type: ScreenInnerContainerComponent, classes: `col-6`,
            data: {
                items: [
                    {
                        type: ScreenContentComponent, classes: `col-12 mb-20`,
                        data: {
                            content: `ATIVIDADE INTERATIVA – 1`,
                            containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
                        }
                    },
                    {
                        type: ScreenCharBalloonComponent, classes: `col-12 mb-20`,
                        data: {
                            charStyle: "full",
                            text: `Vamos fazer uma pausa? Análise sua compreensão do conteúdo visto até aqui realizando uma atividade interativa.`,
                            audio: `./assets/audios/screen_1_35_1.mp3`
                        }
                    }
                ]
            }
        },
        {
            type: ScreenInnerContainerComponent, classes: `col-6 mt-90`,
            hideCurrentAndPreviousOnFinishCount: 2,
            data: {
                items: [
                    {
                        type: ScreenImageComponent, classes: `col-10 offset-1 mb-20`,
                        data: {
                            source: `./assets/images/screen_1_35_1.png`
                        }
                    },
                    {
                        type: ScreenContentComponent, classes: `col-12`,
                        data: {
                            content: `<p><strong>ATIVIDADE INTERATIVA</strong></p>`,
                            containerClasses: `ft-opensans-bold ft-24 color-7 ta-center`
                        }
                    },
                    {
                        type: ScreenImageComponent, classes: `col-12`,
                        data: {
                            source: `./assets/images/screen_shared_start.png`,
                            userShouldClick: true, hideCheck: true, hint: `Clique em Iniciar`
                        }
                    }
                ]
            }
        },
        {
            type: ScreenCustomQuestionTrueOrFalseComponent, classes: `col-12 pdl-30 pdr-30`,
            data: {
                question: `Leia atentamente os trechos com características da 1ª e da 2ª camadas da Gestão de Riscos da AGA. Em seguida, clique na coluna que corresponda à característica analisada. Depois de analisar todas as características, clique em Conferir.`,

                header: {
                    firstColumn: `Características`,
                    secondColumn: `1ª camada`,
                    thirdColumn: `2ª Camada`
                },

                options: [
                    { 
                        text: `1. Produz e mantém um perfil priorizado dos possíveis eventos indesejados da Produção, os controles críticos e as responsabilidades relevantes.`, 
                        correct: true
                    },
                    { 
                        text: `2. WRAC é uma Técnica de Avaliação de Risco e Controle do Ambiente de Trabalho.`, 
                        correct: true
                    },
                    { 
                        text: `3. Identificar e avaliar os riscos relacionados a projetos, mudanças ou problemas. Estabelecer e implantar os controles dos riscos identificados. Ex.: GIM (Guia de Identificação de Mudança), PGM (Plano de Gerenciamento de Mudança) e Análise de Riscos de Mudança.`, 
                        correct: false 
                    },
                    { 
                        text: `4. Deve ser aplicado em mudanças relacionadas a equipamentos, sistemas, ambiente de trabalho, projeto e processos industriais.`, 
                        correct: false 
                    },
                    { 
                        text: `5. A metodologia Bowtie foi elaborada para gerar uma visão geral da situação no qual certos riscos estão presentes e para ajudar as equipes a entenderem a relação entre os riscos e os eventos organizacionais.`, 
                        correct: true
                    }
                ]
            }
        }

    ],
    completed: false
};
