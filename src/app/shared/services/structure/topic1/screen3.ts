import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenAlternatePopupComponent } from 'src/app/shared/components/screen-alternate-popup/screen-alternate-popup.component';


export const t1s3: StructureScreen = {
  title: 'INTRODUÇÃO',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-6 mb-20`,
            data: {
              content: `INTRODUÇÃO`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-11 offset-1`,
            data: {
              charStyle: `full`,
              text: `Você já conhece nosso modelo de Gerenciamento de Riscos? A aplicação desse modelo de camadas é obrigatória para todos os processos e atividades executadas dentro da empresa. Observe.`,
              audio: `./assets/audios/screen_1_3_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-6`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-10 offset-2 mb-10`,
            data: {
              content: `Definição das Camadas`,
              containerClasses: `ft-opensans-bold color-1 ta-center`
            }
          },
          {
            type: ScreenImageComponent, classes: `col-12`,
            data: {
              source: `./assets/images/screen_1_3_1.png`,
              styles: `max-height: unset;`
            }
          }
        ]
      }
    },
  ],
  completed: false
};
