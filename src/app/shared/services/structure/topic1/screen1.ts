import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s1: StructureScreen = {
    title: 'TUTORIAL',
    showInMenu: true,
    topicTitle: topicTitles[0],
    backgroundColor: null,
    backgroundImage: `./assets/images/screen_shared_bg_popup.png`,
    screenClasses: `padding`,
    items: [
        {
            type: ScreenInnerContainerComponent, classes: `col-12 mb-10`,
            data: {
                items: [
                    {
                        type: ScreenContentComponent, classes: `col-3`,
                        data: {
                            content: `TUTORIAL`,
                            containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
                        }
                    },
                ]
            }
        },
        {
            type: ScreenContentComponent, classes: `col-6 offset-3`,
            data: {
                content:
                    `<ul class="no-style">
                        <li class="mb-10">
                            <img src="./assets/images/tutorial-1.png" alt="image"><br>
                            <span class="ft-13">Percentual de conclusão do curso</span>
                        </li>
                        <li class="mb-10">
                            <img src="./assets/images/tutorial-2.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Menu do curso</span>
                            <img src="./assets/images/tutorial-3.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Acesso ao tutorial</span>
                        </li>
                        <li class="mb-10">
                            <img src="./assets/images/tutorial-4.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Botões de navegação</span>
                        </li>
                        <li class="mb-10">
                            <img src="./assets/images/tutorial-5.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Ativar ou desativar áudio</span>
                        </li>
                        <li class="mb-10">
                            <img src="./assets/images/tutorial-6.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Rever tela</span>
                        </li>
                        <li>
                            <img src="./assets/images/tutorial-7.png" alt="image">
                            <span class="ft-13" style="margin: 0 15px 0 10px;">Número de telas</span>
                        </li>
                        <li class="ft-14 mt-10"><strong>Duração aproximada do treinamento: 1 hora.</strong></li>
                    </ul>`,
                containerClasses: `box-6 p-20`
            }
        }
    ],
    completed: false
};
