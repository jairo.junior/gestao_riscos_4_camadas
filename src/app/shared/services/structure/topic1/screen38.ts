import { ScreenAlternateComponent } from 'src/app/shared/components/screen-alternate/screen-alternate.component';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s38: StructureScreen = {
  title: 'GERENCIAMENTO DE RISCO DA 4ª CAMADA ',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-12`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-7 mb-20`,
            data: {
              content: `GERENCIAMENTO DE RISCO DA 4ª CAMADA`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-11`,
            data: {
              charStyle: `full`,
              text: `O gerenciamento de risco da 4ª camada requer a identificação de perigos e a Avaliação de Riscos, para localizar todas as fontes de dano no ambiente, considerar possíveis eventos indesejados e tomar as medidas apropriadas.`,
              audio: `./assets/audios/screen_1_55_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenAlternateComponent, classes: `col-9 offset-2`,
      styles: `position: absolute; top: 265px; max-width: 733px;`,
      data: {
        buttons: [
          `<img src="./assets/images/screen_1_55_1.png" />`,
          `<img src="./assets/images/screen_1_55_2.png" />`,
          `<img src="./assets/images/screen_1_55_3.png" />`,
          `<img src="./assets/images/screen_1_55_4.png" />`,
          `<img src="./assets/images/screen_1_55_5.png" />`,
          `<img src="./assets/images/screen_1_55_6.png" />`,
          `<img src="./assets/images/screen_1_55_7.png" />`
        ],
        checkStyles: [
          `right: 620px;`,
          `right: 517px;`,
          `right: 412px;`,
          `right: 307px;`,
          `right: 203px;`,
          `right: 98px;`,
          `right: 0;`
        ],
        preContent: 
        `<div class="row box-3 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-12">
              <p class="mb-0 ta-center ft-18"><strong>GUIA DE AVALIAÇÃO DE RISCOS</strong></p>
            </div>
        </div>`,
        contentBoxClasses: `ft-14 color-10`,
        contents: [
          `<div class="row box-2 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_55_8.png" />
            </div>
            <div class="col-9 mb-10">
              <p class="mb-10"><strong>1. PARE e pense:</strong> pausar antes de executar uma tarefa.</p>
              <ul class="pdl-20">
                <li>levar em consideração a tarefa a ser executada;</li>
                <li>analisar se é conhecida ou não;</li>
                <li>inspecionar o ambiente, pensar na tarefa como um todo, nos equipamentos e ferramentas, na capacitação necessária e nas regras, procedimentos e regulamentos aplicáveis;</li>
                <li>lembrar de experiências anteriores nesse local e/ou atividade semelhante e considerar as mudanças.</li>
              </ul>
            </div>
           </div>`,

           `<div class="row box-5 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_55_9.png" />
            </div>
            <div class="col-9 mb-10">
              <p class="mb-10"><strong>2. PROCURE e identifique os perigos:</strong> procurar por energias e perigos associados à tarefa</p>
              <ul class="pdl-20">
                <li>Procurar a existência de fontes de energia e condições inseguras envolvidas na tarefa e;</li>
                <li>assegurar que não existem perigos de outras atividades interferindo na sua segurança.</li>
              </ul>
            </div>
           </div>`,

           `<div class="row box-14 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_55_10.png" />
            </div>
            <div class="col-9 mb-10">
              <p class="mb-10"><strong>3. Avalie os efeitos dos perigos: </strong> avaliar os possíveis efeitos dos perigos</p>
              <p>Considerar as possíveis liberações não controladas de energia, os possíveis impactos nas pessoas, no meio ambiente, nas instalações e nos equipamentos, e os controles existentes para proteção.</p>
              <p class="mb-0">É importante entender como as consequências de instalações perigosas causam danos, o que pode ocorrer e a probabilidade da ocorrência.</p>
            </div>
           </div>`,

          `<div class="row box-7 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_55_13.png" />
            </div>
            <div class="col-9 mb-10">
              <p class="mb-10"><strong>4. GERENCIE eventos indesejados com controles apropriados            </strong></p>
              <p class="mb-0">Verifique se existem controles implementados para proteger as pessoas, o meio ambiente, as instalações e os equipamentos dos possíveis danos. Analise se os controles são adequados e avalie as medidas precisas para a execução da tarefa com segurança.</p>
            </div>
           </div>`,
           
          `<div class="row box-8 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-3 ta-center mb-10">
              <img src="./assets/images/screen_1_55_11.png" />
            </div>
            <div class="col-9 mb-10">
              <p class="mb-10"><strong>Verificar se houve mudanças durante a tarefa: </strong>fazer um levantamento do ambiente, procurar por mudanças e executar a tarefa com os controles implementados.</p>
              <p class="mb-0">Observar quaisquer mudanças nas condições desde que a tarefa foi iniciada. Deve-se levar em consideração pessoas e atividades presentes desde que a tarefa foi iniciada, mitigando perigos adicionais.</p>
            </div>
           </div>`,

          `<div class="row box-9 p-20" style="min-height: 235px; margin-top: -1px;">
          <div class="col-3 ta-center mb-10">
            <img src="./assets/images/screen_1_55_12.png" />
          </div>
          <div class="col-9 mb-10">
            <p class="mb-10"><strong>Comunicar os resultados aos colegas depois da tarefa: </strong>comunicar as condições a outras pessoas realizando tarefas similares.</p>
            <p class="mb-0">Identificar outras pessoas que vão executar uma tarefa ou atividade semelhante, comunicar as condições e/ou presença de perigos, e compartilhar experiências sobre a eficácia dos controles. Assim, praticamos o cuidado ativo!</p>
          </div>
           </div>`,

           `<div class="row box-17 p-20" style="min-height: 235px; margin-top: -1px;">
            <div class="col-12">
              <p class="mb-0 ta-center"><strong>Executar a Tarefa com Segurança</strong></p>
            </div>
          </div>`
        ]
      }
    }
  ],
  completed: false
};
