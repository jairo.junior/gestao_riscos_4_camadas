import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s39: StructureScreen = {
  title: 'GERENCIAMENTO DE RISCO DA 4ª CAMADA ',
  showInMenu: false,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_popup.png`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-7 mb-20`,
      data: {
        content: `GERENCIAMENTO DE RISCO DA 4ª CAMADA`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenContentComponent, classes: `col-12 mb-40`,
      data: {
        content: `Até aqui, perpassando por todas as camadas, vimos que a APR é figura imprescindível para que nosso trabalho seja seguro. Então, vamos conhecer melhor este documento.`,
        containerClasses: `color-11 pdl-20 pdr-20`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-5 offset-2`,
      data: {
        source: `./assets/images/screen_1_56_1.png`
      }
    },
    {
      type: ScreenImageComponent, classes: `col-3`,
      data: {
        source: `./assets/images/screen_shared_increase.png`,
        userShouldClick: true,
        checkStyles: `right: -7px; top: 27px;`,
        hint: `Clique no botão Anexo`,
        imageLink: `./assets/attachments/screen_1_56_1.pdf`
      }
    }
  ],
  completed: false
};
