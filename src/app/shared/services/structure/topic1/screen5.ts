import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';
import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';


export const t1s5: StructureScreen = {
  title: 'MEDIDAS DE CONTROLE',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenContentComponent, classes: `col-4 mb-20`,
      data: {
        content: `MEDIDAS DE CONTROLE`,
        containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
      }
    },
    {
      type: ScreenCharBalloonComponent, classes: `col-10 offset-1`,
      data: {
        charStyle: `full`,
        text: `Mas antes que possamos aprender mais sobre as 4 camadas da gestão de riscos, é preciso compreender quais são as medidas de controle para os colaboradores. É importante! Então, preste atenção nas três recomendações abaixo:`,
        audio: `./assets/audios/screen_1_5_1.mp3`
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-8 offset-3`,
      styles: `position: absolute; top: 260px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 bg-color-8 mb-10`,
            data: {
              content: 
              `<div class="row d-flex align-items-center" style="min-height: 65px;">
                <div class="col-2 ta-center">
                  <img src="./assets/images/screen_1_5_1.png" />
                </div>
                <div class="col-10 ft-14" style="line-height: 18px;">
                  <strong>1. CONHEÇA OS RISCOS E MEDIDAS DE CONTROLE</strong><br>

                  Conhecer os riscos e medidas de controle associados com a tarefa, principalmente os controles críticos, que são aqueles que, se falharem, aumentam consideravelmente o risco de acidente grave.
                </div>
              </div>`,
              containerClasses: `p-10 color-10`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 bg-color-3 mb-10`,
            data: {
              content: 
              `<div class="row d-flex align-items-center" style="min-height: 65px;">
                <div class="col-2 ta-center">
                  <img src="./assets/images/screen_1_5_2.png" />
                </div>
                <div class="col-10 ft-14" style="line-height: 18px;">
                  <strong>2. SEMPRE REALIZE A APR AO INÍCIO DA TAREFA</strong><br>

                  Realizar a Análise Preliminar de Riscos (APR) antes do início de cada atividade.
                </div>
              </div>`,
              containerClasses: `p-10 color-1`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12 bg-color-9`,
            data: {
              content: 
              `<div class="row d-flex align-items-center" style="min-height: 65px;">
                <div class="col-2 ta-center">
                  <img src="./assets/images/screen_1_5_3.png" />
                </div>
                <div class="col-10 ft-14" style="line-height: 18px;">
                  <strong>3. EXERÇA SEU DIREITO DE RECUSA</strong><br>

                  Exercer o direito de recusa se os controles críticos da atividade não estiverem implantados.
                </div>
              </div>`,
              containerClasses: `p-10 color-10`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
