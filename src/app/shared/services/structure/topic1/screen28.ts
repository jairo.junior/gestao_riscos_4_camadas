import { ScreenCharBalloonComponent } from 'src/app/shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenImageComponent } from 'src/app/shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from 'src/app/shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenContentComponent } from '../../../components/screen-content/screen-content.component';
import { StructureScreen } from '../../../models/structure-screen.model';
import { topicTitles } from '../topic-titles';


export const t1s28: StructureScreen = {
  title: '3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS',
  showInMenu: true,
  topicTitle: topicTitles[0],
  backgroundColor: null,
  backgroundImage: `./assets/images/screen_shared_bg_leader.jpg`,
  screenClasses: `padding`,
  items: [
    {
      type: ScreenInnerContainerComponent, classes: `col-8`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `3ª CAMADA – GERENCIAMENTO DE RISCOS PARA ATIVIDADES ROTINEIRAS E NÃO ROTINEIRAS`,
              containerClasses: `ft-opensans-bold ft-25 color-1 pdl-20 bg-color-6`
            }
          },
          {
            type: ScreenCharBalloonComponent, classes: `col-9 offset-1`,
            data: {
              charStyle: `full`,
              text: `Os Riscos da Tarefa são o que compõem a Terceira Camada da Gestão de Riscos. Esta etapa vai garantir que toda atividade que oferece riscos a pessoas, processos e meio ambiente tenha um procedimento formal escrito ou uma ART.`,
              audio: `./assets/audios/screen_1_36_1.mp3`
            }
          }
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-4`,
      data: {
        items: [
          {
            type: ScreenImageComponent, classes: `col-12`,
            data: {
              source: `./assets/images/screen_1_36_1.png`,
              styles: `max-height: unset;`
            }
          }          
        ]
      }
    },
    {
      type: ScreenInnerContainerComponent, classes: `col-5 offset-7 pdr-30`,
      styles: `position: absolute; top: 290px;`,
      data: {
        items: [
          {
            type: ScreenContentComponent, classes: `col-12 mb-20`,
            data: {
              content: `Objetivo da Terceira Camada`,
              containerClasses: `ft-opensans-bold color-11`
            }
          },
          {
            type: ScreenContentComponent, classes: `col-12`,
            data: {
              content: 
              `<div class="row">
                <div class="col-12">
                  <p class="mb-0">Definir o processo de gerenciamento de riscos de segurança, saúde e meio ambiente para atividades rotineiras e não rotineiras identificadas na avaliação de riscos de primeira camada da unidade.</p>
                </div>
              </div>`,
              containerClasses: `box-3 color-1`
            }
          }
        ]
      }
    }
  ],
  completed: false
};
