import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/**
 * Serviços responsáveis por delays na execução.
 * @export
 * @class TimeoutService
 */
export class TimeoutService {
  /**
   * IDs dos timeouts em execução.
   * @private
   * @type {Array<number>}
   * @memberof TimeoutService
   */
  private _runningTimeouts: Array<number>;


  /**
   * Construtor.
   * @memberof TimeoutService
   */
  constructor() {
    this._runningTimeouts = new Array<number>();
  }


  /**
   * Para a execução pelo tempo recebido e em seguida executa a ação.
   * @param {(...args: any[]) => void} callback Ação executada ao término do tempo.
   * @param {number} ms Tempo de espera.
   * @param {...any[]} args Parâmetros enviados a ação.
   * @returns {number} ID da ação gerada.
   * @memberof TimeoutService
   */
  set(callback: (...args: any[]) => void, ms: number, ...args: any[]): number {
    const result: number = <any>setTimeout(callback, ms, args);

    this._runningTimeouts.push(result);

    return result;
  }

  /**
   * Cancela a execução do delay de ID recebido.
   * @param {number} id ID que será cancelado.
   * @memberof TimeoutService
   */
  clear (id: number) {
    clearTimeout(id);

    const itemIndex = this._runningTimeouts.indexOf(id);
    if (itemIndex >= 0) {
      this._runningTimeouts.splice(itemIndex, 1);
    }
  }

  /**
   * Cancela todos os delays registrados.
   * @memberof TimeoutService
   */
  clearAll() {
    for (let i = 0; i < this._runningTimeouts.length; i++) {
      clearTimeout(this._runningTimeouts[i]);
    }

    this._runningTimeouts = new Array<number>();
  }
}
