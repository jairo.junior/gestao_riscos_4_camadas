import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/**
 * Serviços responsáveis por renderizar mensagens de orientação.
 * @export
 * @class HintService
 */
export class HintService {
  /**
   * Mensagem de orientação em exibição.
   * @type {string}
   * @memberof HintService
   */
  currentMessage: string;


  /**
   * Exibe a mensagem recebida.
   * @param {string} value Mensagem que será exibida.
   * @memberof HintService
   */
  show(value: string) {
    this.currentMessage = value;
  }

  /**
   * Oculta a mensagem atual.
   * @memberof HintService
   */
  hide() {
    this.currentMessage = null;
  }
}
