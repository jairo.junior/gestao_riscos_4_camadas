import { Injectable } from '@angular/core';

import { ProtocolStatus } from '../models/enums/protocol-status.enum';
import { ProtocolType } from '../models/enums/protocol-type.enum';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

/**
 * Serviços responsáveis pela comunicação com a plataforma LMS.
 * @export
 * @class ProtocolService
 */
export class ProtocolService {
  /**
   * Protocolo de comunicação em uso.
   * @private
   * @type {ProtocolType}
   * @memberof ProtocolService
   */
  private _protocol: ProtocolType;

  /**
   * Define se o serviço foi inicializado.
   * @private
   * @type {boolean}
   * @memberof ProtocolService
   */
  private _initialized: boolean;


  /**
   * Pontuação do usuário no curso.
   * @type {number}
   * @memberof ProtocolService
   */
  score: number;


  /**
   * Construtor.
   * @memberof ProtocolService
   */
  constructor() {
    this._protocol = environment.protocol;
    this._initialized = false;
  }


  /**
   * Inicializa a comunicação com o protocolo.
   * @memberof ProtocolService
   */
  initialize(): void {
    this._initialized = true;

    switch (this._protocol) {
      case ProtocolType.scorm:
        window['loadPage']();
        window.onunload = window['unloadPage'];
        break;
    }

    this.parseRecordedQuestionData();
  }

  /**
   * Carrega o nome do aluno.
   * @readonly
   * @type {string}
   * @memberof ProtocolService
   */
  getStudentName(): string {
    switch (this._protocol) {
      case ProtocolType.scorm:
        return (window['doLMSGetValue']('cmi.core.student_name') || '').toString();
      default:
        return 'Aluno';
    }
  }


  /**
   * Carrega a tela atual do usuário.
   * @readonly
   * @type {number}
   * @memberof ProtocolService
   */
  getCurrentScreen(): number {
    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    }

    const location = this.getLocation();
    return location && location.length > 0 ? parseInt(location.split(';')[0], 10) : 1;
  }

  setCurrentScreen(value: number) {
    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    }

    this.setLocation(value, this.getViewedScreens());
  }

  /**
   * Carrega a lista de status de telas vistas.
   * @type {Array<number>}
   * @memberof ProtocolService
   */
  getViewedScreens(): Array<number> {
    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    }

    const location = this.getLocation();
    const brokenLocation = location.indexOf(';') >= 0 ? location.split(';')[1] : '';
    return brokenLocation && brokenLocation.length > 0 ? brokenLocation.split('').map(x => parseInt(x, 10)) : new Array<number>();
  }

  setViewedScreens(value: Array<number>) {
    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    }

    this.setLocation(this.getCurrentScreen(), value);

    if (value.indexOf(0) === -1) {
      this.setStatus(ProtocolStatus.completed);
    }
  }

  /**
   * Pontuação do aluno no tópico.
   * @type {Array<number>}
   * @memberof ProtocolService
   */
  getScore(): number {

    let score = 0;

    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    }

    switch (this._protocol) {
      case ProtocolType.scorm:
        score = (window['doLMSGetValue']('cmi.core.score.raw') || 0);
        break;
    }

    return score;
  }

  setScore(value: number) {
    if (!this._initialized) {
      throw new Error('ProtocolService not initialized.');
    };

    switch (this._protocol) {
      case ProtocolType.scorm:
        window['doLMSSetValue']('cmi.core.score.raw', value);
        break;
    }
  }


  /**
   * Carrega as informações de visualização de telas do banco.
   * @private
   * @returns {string} Valor carregado do banco.
   * @memberof ProtocolService
   */
  private getLocation(): string {
    let location = '';

    switch (this._protocol) {
      case ProtocolType.scorm:
        location = (window['doLMSGetValue']('cmi.core.lesson_location') || '').toString();
        break;
    }

    return location || '';
  }

  /**
   * Grava as informações de visualização de telas no banco.
   * @private
   * @param {number} current Tela atual que será gravada.
   * @param {Array<number>} viewed Lista de status de telas vistas que será gravado.
   * @memberof ProtocolService
   */
  private setLocation(current: number, viewed: Array<number>) {
    const location = current + ';' + viewed.join('');

    switch (this._protocol) {
      case ProtocolType.scorm:
        window['doLMSSetValue']('cmi.core.lesson_location', location);
        break;
    }
  }


  /**
   * Carrega o status de andamento do banco.
   * @private
   * @returns {ProtocolStatus} Valor carregado do banco.
   * @memberof ProtocolService
   */
  private getStatus(): ProtocolStatus {
    let status = ProtocolStatus.notStarted;
    switch (this._protocol) {
      case ProtocolType.scorm:
        const scormStatus = (window['doLMSGetValue']('cmi.core.lesson_status') || '').toString();
        switch (scormStatus) {
          case 'not attempted':
            status = ProtocolStatus.notStarted;
            break;

          case 'incomplete':
            status = ProtocolStatus.started;
            break;

          case 'completed':
            status = ProtocolStatus.completed;
            break;
        }
        break;
    }

    return status;
  }

  /**
   * Grava o status do tópico no banco.
   * @private
   * @param {ProtocolStatus} value Status do tópico.
   * @memberof ProtocolService
   */
  private setStatus(value: ProtocolStatus) {
    switch (this._protocol) {
      case ProtocolType.scorm:
        switch (value) {
          case ProtocolStatus.notStarted:
            window['doLMSSetValue']('cmi.core.lesson_status', 'not attempted');
            break;

          case ProtocolStatus.started:
            window['doLMSSetValue']('cmi.core.lesson_status', 'incomplete');
            break;

          case ProtocolStatus.completed:
            window['doLMSSetValue']('cmi.core.lesson_status', 'completed');
            break;
        }

        window['doLMSCommit']('');
        break;
    }
  }


  /**
   * Carrega os dados extras do banco.
   * @private
   * @returns {any} Valor carregado do banco.
   * @memberof ProtocolService
   */
  getExtraData(): any {
    switch (this._protocol) {
      case ProtocolType.scorm:
        return JSON.parse((window['doLMSGetValue']('cmi.suspend_data') || '{}').toString());

      default:
        return {};
    }
  }

  /**
   * Grava os dados extras no banco.
   * @private
   * @param {any} value Valor que será gravado.
   * @memberof ProtocolService
   */
  setExtraData(value: any) {
    switch (this._protocol) {
      case ProtocolType.scorm:
        window['doLMSSetValue']('cmi.suspend_data', JSON.stringify(value));
        window['doLMSCommit']('');
        break;
    }

    this.parseRecordedQuestionData();
  }


  /**
   * Carrega as informações sobre resposta da questão do banco.
   * @private
   * @memberof ScreenExamFeedbackComponent
   */
  private parseRecordedQuestionData() {
    this.score = 1200;

    const extraData = this.getExtraData();
    if (extraData.hasOwnProperty('exam')) {
      const examData = extraData['exam'] as Array<any>;

      for (let i = 0; i < examData.length; i++) {
        const examQuestionData = examData[i];

        this.score += examQuestionData.score;
      }
    }
  }
}
