import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { ScreenItem } from '../../base/screen-item.component';
import { fadeAnimation } from './../../animations/animations';
import { AudioService } from './../../services/audio.service';

@Component({
  selector: 'app-screen-char-balloon',
  templateUrl: './screen-char-balloon.component.html',
  styleUrls: ['./screen-char-balloon.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenCharBalloonComponent extends ScreenItem implements OnInit, OnDestroy {
  /**
   * Instância do personagem completo.
   * @private
   * @static
   * @type {*}
   * @memberof ScreenCharBalloonComponent
   */
  private static _charFull: any;

  /**
   * Instância do personagem completo.
   * @private
   * @static
   * @type {*}
   * @memberof ScreenCharBalloonComponent
   */
  private static _charPartial: any;


  /**
   * Estilo aplicado ao personagem.
   * @type {string}
   * @memberof ScreenCharBalloonComponent
   */
  charStyle: string;

  /**
   * Texto exibido no balão.
   * @type {string}
   * @memberof ScreenCharBalloonComponent
   */
  text: string;


  /**
   * Define se o componente foi inicializado.
   * @private
   * @type {boolean}
   * @memberof ScreenCharBalloonComponent
   */
  private _initialized: boolean;

  /**
   * Define se o componente já deve reproduzir ao ser inicializado.
   * @private
   * @type {boolean}
   * @memberof ScreenCharBalloonComponent
   */
  private _waitingForPlay: boolean;

  /**
   * Arquivo de áudio.
   * @type {string}
   * @memberof ScreenCharBalloonComponent
   */
  private _audio: string;

  /**
   * Ouvinte de evento de início de reprodução do serviço de áudio.
   * @private
   * @type {Subscription}
   * @memberof ScreenCharBalloonComponent
   */
  private _audioPlaySubscription: Subscription;

  /**
   * Ouvinte de evento de término de reprodução do serviço de áudio.
   * @private
   * @type {Subscription}
   * @memberof ScreenCharBalloonComponent
   */
  private _audioFinishSubscription: Subscription;


  /**
   * Construtor.
   * @param {ElementRef} _elementRef Referência do elemento DOM.
   * @param {AudioService} audioService Serviços responsáveis pela reprodução de áudios.
   * @memberof ScreenCharBalloonComponent
   */
  constructor(private _elementRef: ElementRef, public audioService: AudioService) {
    super();
  }


  /**
   * Inicializa recursos do componente.
   * @memberof ScreenCharBalloonComponent
   */
  async ngOnInit(): Promise<void> {
    if (ScreenCharBalloonComponent._charFull === null || ScreenCharBalloonComponent._charFull === undefined) {
      const canvasFull = document.createElement('canvas');
      canvasFull.width = 173;
      canvasFull.height = 371;

      ScreenCharBalloonComponent._charFull = await this.buildCharStyle(canvasFull, '303D236A4F9C4440B6EA8C74CEDB1FE3', 'char_full');
    }

    this._elementRef.nativeElement.childNodes[0].prepend(ScreenCharBalloonComponent._charFull.canvas);

    if (ScreenCharBalloonComponent._charPartial === null || ScreenCharBalloonComponent._charPartial === undefined) {
      const canvasPartial = document.createElement('canvas');
      canvasPartial.width = 177;
      canvasPartial.height = 205;

      ScreenCharBalloonComponent._charPartial = await this.buildCharStyle(canvasPartial, 'A41A16D218B4D844B6A8D18608E897F6', 'char_partial');
    }

    this._elementRef.nativeElement.childNodes[0].prepend(ScreenCharBalloonComponent._charPartial.canvas);

    this._initialized = true;

    if (this._waitingForPlay) {
      this.appendChar();

      this.playAudio();
    }
  }

  /**
   * Finaliza recursos do componente.
   * @memberof ScreenCharBalloonComponent
   */
  ngOnDestroy(): void {
    this.removeChar();
  }

  /**
   * Constrói a animação no canvas recebido.
   * @private
   * @param {Element} canvas Canvas onde será construída.
   * @param {string} composition Identificador do componente.
   * @param {string} exportRootName Nome da classe do componente.
   * @returns {Promise<any>} Promise com o objeto renderizado no palco.
   * @memberof ScreenCharBalloonComponent
   */
  private buildCharStyle(canvas: Element, composition: string, exportRootName: string): Promise<any> {
    return new Promise(function(resolve) {
      const comp = window['AdobeAn'].getComposition(composition);
      const lib = comp.getLibrary();
      
      const buildAndResolve = function() {
        const exportRoot = new lib[exportRootName]();
        const stage = new lib.Stage(canvas);

        window['AdobeAn'].compositionLoaded(lib.properties.id);

        stage.addChild(exportRoot);
        window['createjs'].Ticker.setFPS(lib.properties.fps);
        window['createjs'].Ticker.addEventListener('tick', stage);

        stage.stop();
        stage.seek(0);

        resolve(stage);
      };

      // Caso tenha assets para serem carregados.
      if (lib.properties.manifest.length > 0) {
        const loader = new window['createjs'].LoadQueue(false);
        loader.addEventListener('fileload', function(evt) {
          const images = comp.getImages();

          if (evt && (evt.item.type === 'image')) {
            images[evt.item.id] = evt.result;
          }
        });

        loader.addEventListener('complete', function(evt) {
          const ss = comp.getSpriteSheet();
          const queue = evt.target;
          const ssMetadata = lib.ssMetadata;

          for (let i = 0; i < ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new window['createjs'].SpriteSheet({
              'images': [queue.getResult(ssMetadata[i].name)],
              'frames': ssMetadata[i].frames
            });
          }

          buildAndResolve();
        });

        loader.loadManifest(lib.properties.manifest);
      } else {
        buildAndResolve();
      }
    });
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenItem
   */
  parseData(value: any) {
    this.charStyle = value.charStyle || 'partial';
    this.text = value.text;

    this._audio = value.audio;

    this.appendChar();
  }

  /**
   * Inicia a execução do item.
   * @memberof ScreenItem
   */
  start() {
    if (this._initialized) {
      this.playAudio();
    } else {
      this._waitingForPlay = true;
    }
  }

  /**
   * Alterna o status de execução do áudio.
   * @memberof ScreenCharBalloonComponent
   */
  toggleAudio() {
    if (!this.audioService.playing) {
      this.playAudio();
    } else {
      this.stopAudio();
    }
  }

  /**
   * Reproduz o áudio do elemento.
   * @memberof ScreenCharBalloonComponent
   */
  playAudio() {
    if (this.audioService.isActive) {
      this._audioPlaySubscription = this.audioService.started.subscribe(() => this.playChar());
      this._audioFinishSubscription = this.audioService.finished.subscribe(() => this.onAudioFinished());

      this.audioService.play(this._audio);

      this.finish();
    } else {
      this.onAudioFinished();

      this.finish();
    }
  }

  /**
   * Finaliza reprodução do áudio.
   * @memberof ScreenCharBalloonComponent
   */
  stopAudio() {
    if (this._audioPlaySubscription !== null && this._audioPlaySubscription !== undefined) {
      this._audioPlaySubscription.unsubscribe();
    }

    if (this._audioFinishSubscription !== null && this._audioFinishSubscription !== undefined) {
      this._audioFinishSubscription.unsubscribe();
    }

    if (this.audioService.isActive && this.audioService.playing) {
      this.audioService.stop();
    }

    this.onAudioFinished();
  }

  /**
   * Insere o personagem na página.
   * @private
   * @memberof ScreenCharBalloonComponent
   */
  private appendChar() {
    if (ScreenCharBalloonComponent._charFull === null || ScreenCharBalloonComponent._charFull === undefined ||
        ScreenCharBalloonComponent._charPartial === null || ScreenCharBalloonComponent._charPartial === undefined) {
      return;
    }

    this.removeChar();

    if (this.charStyle === 'full') {
      this._elementRef.nativeElement.childNodes[0].prepend(ScreenCharBalloonComponent._charFull.canvas);
    } else if (this.charStyle === 'partial') {
      this._elementRef.nativeElement.childNodes[0].prepend(ScreenCharBalloonComponent._charPartial.canvas);
    }
  }

  /**
   * Remove o personagem da página.
   * @private
   * @memberof ScreenCharBalloonComponent
   */
  private removeChar() {
    if (ScreenCharBalloonComponent._charFull === null || ScreenCharBalloonComponent._charFull === undefined ||
        ScreenCharBalloonComponent._charPartial === null || ScreenCharBalloonComponent._charPartial === undefined) {
      return;
    }

    if (ScreenCharBalloonComponent._charFull.canvas.parentNode !== null) {
      this._elementRef.nativeElement.childNodes[0].removeChild(ScreenCharBalloonComponent._charFull.canvas);
    }

    if (ScreenCharBalloonComponent._charPartial.canvas.parentNode !== null) {
      this._elementRef.nativeElement.childNodes[0].removeChild(ScreenCharBalloonComponent._charPartial.canvas);
    }
  }

  /**
   * Executa a animação do personagem.
   * @private
   * @memberof ScreenCharBalloonComponent
   */
  private playChar() {
    switch (this.charStyle) {
      case 'full':
        ScreenCharBalloonComponent._charFull.seek(0);
        ScreenCharBalloonComponent._charFull.play();
        break;

      case 'partial':
      ScreenCharBalloonComponent._charPartial.seek(0);
      ScreenCharBalloonComponent._charPartial.play();
        break;
    }
  }

  /**
   * Para a animação do personagem.
   * @private
   * @memberof ScreenCharBalloonComponent
   */
  private stopChar() {
    switch (this.charStyle) {
      case 'full':
      ScreenCharBalloonComponent._charFull.stop();
        ScreenCharBalloonComponent._charFull.seek(0);
        break;

      case 'partial':
      ScreenCharBalloonComponent._charPartial.stop();
      ScreenCharBalloonComponent._charPartial.seek(0);
        break;
    }
  }

  /**
   * Realiza validações no término da reprodução.
   *
   * @private
   * @memberof ScreenCharBalloonComponent
   */
  private onAudioFinished() {
    this.stopChar();

    if (this._audioFinishSubscription !== null && this._audioFinishSubscription !== undefined) {
      this._audioFinishSubscription.unsubscribe();
    }

    //this.finish();
  }
}
