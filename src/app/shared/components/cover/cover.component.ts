import { Component, EventEmitter, Output } from '@angular/core';

import { fadeAnimation } from './../../animations/animations';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente que exibe a capa do curso.
 * @export
 * @class CoverComponent
 */
export class CoverComponent {
  /**
   * Emissor do evento que inicia curso.
   * @type {EventEmitter}
   * @memberof CoverComponent
   */
  @Output()
  startCourse: EventEmitter<object>;


  /**
   * Construtor.
   * @memberof CoverComponent
   */
  constructor() {
    this.startCourse = new EventEmitter();
  }


  /**
   * Dispara evento que inicia o curso.
   * @memberof MenuComponent
   */
  dispatchStartCourse(): void {
    this.startCourse.emit({});
  }
}