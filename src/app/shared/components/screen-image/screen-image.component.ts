import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';
import { HintService } from './../../services/hint.service';

@Component({
  selector: 'app-screen-image',
  templateUrl: './screen-image.component.html',
  styleUrls: ['./screen-image.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por renderizar imagens das páginas.
 * @export
 * @class ScreenTitleComponent
 * @extends {ScreenItem}
 */
export class ScreenImageComponent extends ScreenItem {
  /**
   * Imagem exibida no conteúdo.
   * @type {string}
   * @memberof ScreenImageComponent
   */
  source: string;

  /**
   * Classes aplicadas ao componente de imagem.
   * @type {string}
   * @memberof ScreenImageComponent
   */
  classes: string;

  /**
   * Estilos aplicados ao componente de imagem.
   * @type {string}
   * @memberof ScreenImageComponent
   */
  styles: string;

  /**
   * Define se o usuário deve clicar na imagem para a interação finalizar.
   * @type {boolean}
   * @memberof ScreenImageComponent
   */
  userShouldClick: boolean;

  /**
   * Define se o item deve ser finalizado antes do usuário clicar.
   * @type {boolean}
   * @memberof ScreenImageComponent
   */
  finishBeforeClick: boolean;

  /**
   * Estilos CSS aplicados aos marcadores.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  checkStyles: string;

  /**
   * Define se o marcador de conclusão será ocultado.
   * @type {boolean}
   * @memberof ScreenImageComponent
   */
  hideCheck: boolean;

  /**
   * Mensagem de orientação exibida para o clique.
   * @type {String}
   * @memberof ScreenImageComponent
   */
  hint: string;

  /**
   * Link aberto ao clicar na imagem.
   * @type {string}
   * @memberof ScreenImageComponent
   */
  imageLink: string;

  /**
   * Define se o item já foi clicado.
   * @type {boolean}
   * @memberof ScreenImageComponent
   */
  clicked: boolean;

  /**
   * Construtor.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenImageComponent
   */
  constructor(public hintService: HintService, private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenImageComponent
   */
  parseData(value: any) {
    this.source = value.source;
    this.classes = value.classes;
    this.styles = value.styles;
    this.userShouldClick = value.userShouldClick;
    this.finishBeforeClick = value.finishBeforeClick;
    this.checkStyles = value.checkStyles;
    this.hideCheck = value.hideCheck;
    this.hint = value.hint != null && value.hint != undefined ? value.hint : `Clique na imagem`;
    this.imageLink = value.imageLink;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenImageComponent
   */
  start() {
    if (!this.userShouldClick || this.finishBeforeClick) {
      this.timeoutService.set(() => this.finish(), 500);
    } else {
      this.hintService.show(this.hint);
    }
  }

  /**
   * Finaliza a interação após clique do usuário.
   * @memberof ScreenImageComponent
   */
  onUserClick() {
    if (!this.userShouldClick) {
      return;
    }

    if (this.imageLink !== null && this.imageLink !== undefined && this.imageLink.length > 0) {
      window.open(this.imageLink, '_blank', 'toolbar=0,location=0,menubar=0');
    }

    this.clicked = true;

    if (!this.finishBeforeClick) {
      this.hintService.hide();
      this.finish();
    }
  }
}
