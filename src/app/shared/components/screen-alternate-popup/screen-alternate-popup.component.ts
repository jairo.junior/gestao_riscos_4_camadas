import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { HintService } from '../../services/hint.service';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

@Component({
  selector: 'app-screen-alternate-popup',
  templateUrl: './screen-alternate-popup.component.html',
  styleUrls: ['./screen-alternate-popup.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por renderizar itens clicáveis alternados.
 * @export
 * @class ScreenAlternatePopupComponent
 * @extends {ScreenItem}
 */
export class ScreenAlternatePopupComponent extends ScreenItem {
  /**
   * Posição padrão para as checks de conclusão.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  readonly DEFAULT_CHECK_POSITION: string = `top right`;


  /**
   * Posição do item em exibição.
   * @private
   * @type {number}
   * @memberof ScreenAlternatePopupComponent
   */
  private _currentIndex: number;


  /**
   * Título do conteúdo exibido.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  currentTitle: string;

  /**
   * Texto do conteúdo exibido.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  currentText: string;

  /**
   * Quadro em exibição no momento.
   * @type {number}
   * @memberof ScreenAlternatePopupComponent
   */
  frame: number;

  /**
   * Define se está ocorrendo navegação.
   * @type {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  navigating: boolean;

  /**
   * Define se o popup pode ser fechado.
   * @type {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  canClose: boolean;


  /**
   * Define se os botões usam estrutura de container HTML.
   *
   * @type {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  useContainer: boolean;

  /**
   * Tamanho aplicado as linhas do container.
   *
   * @type {number}
   * @memberof ScreenAlternatePopupComponent
   */
  containerSize: number;

  /**
   * Classes aplicadas ao container da caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  contentAreaClasses: string;

  /**
   * Estilos aplicados ao container da caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  contentAreaStyles: string;

  /**
   * Classes aplicadas a caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  contentBoxClasses: string;

  /**
   * Estilos aplicados a caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  contentBoxStyles: string;

  /**
   * Nome do arquivo utilizado como check.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  checkName: string;

  /**
   * Posição onde será exibido o check de conclusão do item.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  checkPosition: string;

  /**
   * Estilos CSS aplicados aos marcadores separadamente.
   * @type {Array<string>}
   * @memberof ScreenAlternatePopupComponent
   */
  checkStyles: Array<string>;

  /**
   * Mensagem de orientação customizada.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  customHint: string;

  /**
   * Textos dos botões exibidos.
   * @type {Array<string>}
   * @memberof ScreenAlternatePopupComponent
   */
  buttons: Array<string>;

  /**
   * Classes aplicadas aos botões para centralizar o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  buttonsClasses: string[];

  /**
   * Classes aplicadas aos botões para centralizar o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  buttonsStyles: string[];

  /**
   * Conteúdo dos botões.
   * @type {Array<string>}
   * @memberof ScreenAlternatePopupComponent
   */
  contents: Array<string | string[]>;

  /**
   * Classes aplicadas ao botão fechar para cada conteúdo.
   * @type {string[]}
   * @memberof ScreenAlternatePopupComponent
   */
  closeButtonClasses: string[];

  /**
   * Estilos aplicados ao botão fechar para cada conteúdo.
   * @type {string[]}
   * @memberof ScreenAlternatePopupComponent
   */
  closeButtonStyles: string[];

  /**
   * Estilos aplicados ao botão de voltar da navegação.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  backButtonStyles: string;

  /**
   * Estilos aplicados ao botão de avançar da navegação.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  nextButtonStyles: string;

  /**
   * Status de visualização de cada item.
   * @type {Array<boolean>}
   * @memberof ScreenAlternatePopupComponent
   */
  status: Array<boolean>;

  /**
   * Mensagem de orientação exibida para itens de navegação.
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  navHint: string;

  /**
   * Construtor.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenAlternatePopupComponent
   */
  constructor(public hintService: HintService, private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Classes aplicadas ao botão fechar.
   * @readonly
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  get currentCloseButtonClasses(): string {
    return this._currentIndex >= 0 ? this.closeButtonClasses[this._currentIndex] : '';
  }

  /**
   * Estilos aplicados ao botão fechar.
   * @readonly
   * @type {string}
   * @memberof ScreenAlternatePopupComponent
   */
  get currentCloseButtonStyles(): string {
    return this._currentIndex >= 0 ? this.closeButtonStyles[this._currentIndex] : '';
  }



  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenAlternatePopupComponent
   */
  parseData(value: any) {
    this.useContainer = value.useContainer;
    this.containerSize = value.containerSize;
    this.contentAreaClasses = value.contentAreaClasses;
    this.contentAreaStyles = value.contentAreaStyles;
    this.contentBoxClasses = value.contentBoxClasses;
    this.contentBoxStyles = value.contentBoxStyles;
    this.checkName = value.checkName || `./assets/svgs/check.svg`;
    this.checkPosition = value.checkPosition || this.DEFAULT_CHECK_POSITION;
    this.checkStyles = value.checkStyles;
    this.customHint = value.customHint;
    this.buttons = value.buttons;
    this.buttonsClasses = value.buttonsClasses || [];
    this.buttonsStyles = value.buttonsStyles || [];
    this.contents = value.contents;
    this.navHint = value.navHint || `Clique na seta`;

    this.closeButtonClasses = this.parseListData(value.closeButtonClasses, this.buttons.length);
    this.closeButtonStyles = this.parseListData(value.closeButtonStyles, this.buttons.length);

    this.backButtonStyles = value.backButtonStyles;
    this.nextButtonStyles = value.nextButtonStyles;

    if (this.checkStyles === null || this.checkStyles === undefined) {
      this.checkStyles = new Array<string>();
    }

    if (this.checkStyles.length < this.buttons.length) {
      while (this.checkStyles.length < this.buttons.length) {
        this.checkStyles.push('');
      }
    }
  }

  /**
   * Gera uma lista da propriedade recebida para atender a todos os popups.
   * @private
   * @param {*} value Valor que será lido.
   * @param {number} count Quantidade de posição que a lista deverá possuir.
   * @returns {string[]} Lista gerada.
   * @memberof ScreenAlternatePopupComponent
   */
  private parseListData(value: any, count: number): string[] {
    let result: string[] = [];

    if (Array.isArray(value)) {
      result = value;

      while (result.length < count) {
        result.push('');
      }
    } else  {
      for (let i = 0; i < count; i++) {
        result.push(value + '' || '');
      }
    }

    return result;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenAlternatePopupComponent
   */
  start() {
    this.status = new Array<boolean>();

    for (let i = 0; i < this.buttons.length; i++) {
      this.status.push(false);
    }

    this.showHintMessage();
  }


  /**
   * Visualiza o item clicado.
   * @param {number} index Posição do item clicado na lista.
   * @memberof ScreenAlternatePopupComponent
   */
  view(index: number) {
    if (!this.isFinished) {
      this.hintService.hide();
    }

    this._currentIndex = index;

    this.frame = 0;
    this.currentTitle = this.buttons[index];
    this.currentText = this.hasNavigation ? this.contents[index][this.frame].toString() : this.contents[index].toString();
    this.status[index] = true;

    if (!this.hasNavigation) {
      if (this.status.indexOf(false) === -1) {
        this.finish();
      }

      this.prepareClose();
    } else if (!this.isFinished) {
      this.hintService.show(this.navHint);
    }
  }

  /**
   * Valida se o item da posição recebida deve ser marcado com o check.
   * @param {number} index Posição da lista que será validada.
   * @memberof ScreenAlternatePopupComponent
   */
  isChecked(index: number) {
    return this.status[index] === true;
  }


  /**
   * Valida se o conteúdo possui navegação.
   * @returns {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  get hasNavigation(): boolean {
    return this._currentIndex >= 0 ? Array.isArray(this.contents[this._currentIndex]) : false;
  }

  /**
   * Valida se é possível voltar na navegação.
   * @returns {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  canGoBack(): boolean {
    return this.frame >= 0 && this.frame > 0;
  }

  /**
   * Valida se é possível avançar na navegação.
   * @returns {boolean}
   * @memberof ScreenAlternatePopupComponent
   */
  canGoNext(): boolean {
    return this.frame >= 0 && this.frame < this.contents[this._currentIndex].length - 1;
  }

  /**
   * Navega no conteúdo.
   * @param {boolean} goNext Define se a navegação está avançando.
   * @memberof ScreenAlternatePopupComponent
   */
  navigate(goNext: boolean): void {
    if (this.frame < 0) {
      return;
    }

    if (!this.isFinished && this.hintService.currentMessage == this.navHint) {
      this.hintService.hide();
    }

    this.navigating = true;
    this.frame = this.frame + (goNext ? +1 : -1);
    this.currentText = null;

    this.timeoutService.set(() => {
      this.navigating = false;
      this.currentText = this.contents[this._currentIndex][this.frame].toString();
  
      if (!this.canGoNext()) {
        if (this.status.indexOf(false) === -1) {
          this.finish();
        }

        this.prepareClose();
      } else if (!this.isFinished && this.hintService.currentMessage == null) {
        this.hintService.show(this.navHint);
      }
    }, 300);
  }


  /**
   * Prepara o botão fechar.
   * @memberof ScreenAlternatePopupComponent
   */
  prepareClose(): void {
    this.canClose = true;

    if (!this.isFinished) {
      this.hintService.show('Clique em fechar');
    }
  }

  /**
   * Fecha o conteúdo em exibição.
   * @memberof ScreenAlternatePopupComponent
   */
  close() {
    if (!this.isFinished) {
      this.hintService.hide();
    }

    this.canClose = false;
    this._currentIndex = -1;
    this.currentText = null;

    this.showHintMessage();
  }


  /**
   * Exibe a mensagem de orientação para abertura dos popups.
   * @private
   * @memberof ScreenAlternatePopupComponent
   */
  private showHintMessage() {
    if (!this.isFinished) {
      const hasCustomMessage = this.customHint !== null && this.customHint !== undefined && this.customHint.length > 0;
      this.hintService.show(hasCustomMessage ? this.customHint : 'Clique sobre os botões');
    }
  }
}
