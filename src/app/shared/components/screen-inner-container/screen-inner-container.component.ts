import { Component, ComponentFactory, ComponentFactoryResolver, Renderer2, ViewChild } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { StructureScreenItemData } from '../../models/structure-screen-item-data';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';
import { ViewHostDirective } from './../../directives/view-host.directive';

@Component({
  selector: 'app-screen-inner-container',
  templateUrl: './screen-inner-container.component.html',
  styleUrls: ['./screen-inner-container.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por criar container interno de itens.
 * @export
 * @class ScreenInnerContainerComponent
 * @extends {ScreenItem}
 */
export class ScreenInnerContainerComponent extends ScreenItem {
  /**
   * Elemento responsável por renderizar os componentes em tempo de execução.
   * @type {ViewHostDirective}
   * @memberof ScreenComponent
   */
  @ViewChild(ViewHostDirective)
  appViewHost: ViewHostDirective;


  /**
   * Classes aplicadas ao container.
   * @type {string}
   * @memberof ScreenInnerContainerComponent
   */
  public classes: string;

  /**
   * Estilos aplicados ao container.
   * @type {string}
   * @memberof ScreenInnerContainerComponent
   */
  public styles: string;


  /**
   * Posição do item em execução na lista.
   * @private
   * @type {number}
   * @memberof ScreenComponent
   */
  private _runningItemIndex: number;

  /**
   * Lista de itens que serão renderizados
   * @private
   * @type {Array<ScreenItem>}
   * @memberof ScreenInnerContainerComponent
   */
  private _items: Array<StructureScreenItemData>;

  /**
   * Factories para os componentes das telas.
   * @private
   * @type {Array<ComponentFactory<ScreenItem>>}
   * @memberof ScreenComponent
   */
  private _itemsFactories: Array<ComponentFactory<ScreenItem>>;


  /**
   * Construtor.
   * @memberof ScreenInnerContainerComponent
   * @param {ComponentFactoryResolver} componentFactoryResolver Factory responsável pela criação de instâncias de componentes.
   * @param {Renderer2} renderer2 Utilitário para trabalhar com o DOM dos componentes.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   */
  constructor(private componentFactoryResolver: ComponentFactoryResolver, private renderer2: Renderer2,
              private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenInnerContainerComponent
   */
  parseData(value: any) {
    this._items = value.items;
    this.classes = value.classes;
    this.styles = value.styles;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenInnerContainerComponent
   */
  start() {
    const viewContainer = this.appViewHost.viewContainerRef;
    viewContainer.clear();

    this._runningItemIndex = -1;
    this._itemsFactories = [];

    for (let index = 0; index < this._items.length; index++) {
      const itemData = this._items[index];
      this._itemsFactories.push(this.componentFactoryResolver.resolveComponentFactory(itemData.type));
    }

    this.buildAndRunItem();
  }

  /**
   * Constrói a referência do componente e executa o mesmo.
   * @private
   * @memberof ScreenComponent
   */
  private buildAndRunItem() {
    this._runningItemIndex++;

    const viewContainer = this.appViewHost.viewContainerRef;
    const itemData = this._items[this._runningItemIndex];
    const itemRef = viewContainer.createComponent(this._itemsFactories[this._runningItemIndex]);

    if (itemData.classes) {
      let splittedClasses: string[] = itemData.classes.split(" ");
      for (let i = 0; i < splittedClasses.length; i++) {
        this.renderer2.addClass(itemRef.location.nativeElement, splittedClasses[i]);
      }
    }

    if (itemData.styles) {
      let splittedStyles: string[] = itemData.styles.split(";");
      for (let i = 0; i < splittedStyles.length; i++) {
        if (splittedStyles[i]) {
          let brokenStyle: string[] = splittedStyles[i].split(":");

          this.renderer2.setStyle(itemRef.location.nativeElement, brokenStyle[0].trim(), brokenStyle[1].trim());
        }
      }
    }

    itemRef.instance.data = itemData.data;
    itemRef.instance.finished.subscribe((e) => this.onItemFinished(e));
    itemRef.instance.start();
  }

  private onItemFinished(e: any) {
    let action: (...args: any[]) => void = () => {
      if (this._runningItemIndex === this._items.length - 1) {
        this.finish();
      } else {
        this.timeoutService.set(() => this.buildAndRunItem(), 200);
      }
    };

    const itemData = this._items[this._runningItemIndex];
    if (itemData.hideCurrentAndPreviousOnFinishCount != null && itemData.hideCurrentAndPreviousOnFinishCount > 0) {
      const viewContainer = this.appViewHost.viewContainerRef;
      for (let i = 0; i < itemData.hideCurrentAndPreviousOnFinishCount; i++) {
        viewContainer.remove(viewContainer.length - 1);
      }

      this.timeoutService.set(action, 250);
    } else {
      action();
    }
  }
}
