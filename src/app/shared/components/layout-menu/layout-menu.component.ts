import { Component, EventEmitter, Output } from '@angular/core';

import { fadeAnimation } from '../../animations/animations';
import { StructureService } from '../../services/structure.service';

@Component({
  selector: 'app-layout-menu',
  templateUrl: './layout-menu.component.html',
  styleUrls: ['./layout-menu.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por renderizar o menu do curso.
 * @export
 * @class LayoutMenuComponent
 */
export class LayoutMenuComponent {
  /**
   * Emissor do evento que define que o menu foi fechado.
   * @type {EventEmitter}
   * @memberof LayoutMenuComponent
   */
  @Output()
  close: EventEmitter<object>;


  /**
   * Construtor.
   * @param {StructureService} structureService Serviços responsáveis por disponibilizar a estrutura do curso.
   * @memberof LayoutMenuComponent
   */
  constructor(public structureService: StructureService) {
    this.close = new EventEmitter();
  }


  /**
   * Dispara evento de fechamento do componente.
   * @memberof LayoutTutorialComponent
   */
  dispatchClose(): void {
    this.close.emit({});
  }

  /**
   * Tenta acessar a tela.
   * @param {number} screen Tela que será acessada.
   * @memberof LayoutMenuComponent
   */
  tryAccessScreen(screen: number) {
    if (this.structureService.canAccess(screen)) {
      this.structureService.tryAccess(screen);
      this.dispatchClose();
    }
  }
}
