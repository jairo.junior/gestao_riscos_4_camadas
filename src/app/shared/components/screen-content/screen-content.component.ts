import { Component, ElementRef, ViewChild } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';
import { HintService } from './../../services/hint.service';
import { ProtocolService } from './../../services/protocol.service';

@Component({
  selector: 'app-screen-content',
  templateUrl: './screen-content.component.html',
  styleUrls: ['./screen-content.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente que renderiza contéudo HTML na página.
 * @export
 * @class ScreenContentComponent
 * @extends {ScreenItem}
 */
export class ScreenContentComponent extends ScreenItem {
  /**
   * Mensagem de orientação padrão utilizada para links.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  readonly DEFAULT_LINK_HINT_SINGLE: string = 'Clique no ícone de PDF';

  /**
   * Mensagem de orientação padrão utilizada para links.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  readonly DEFAULT_LINK_HINT_MULTI: string = 'Clique nos ícones de PDF';


  @ViewChild('container')
  container:ElementRef<HTMLDivElement>;


  /**
   * Conteúdo HTML renderizado.
   * @type {string[]}
   * @memberof ScreenContentComponent
   */
  content: string[];

  /**
   * Mensagem de orientação exibida para itens de navegação.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  navHint: string;

  /**
   * Quadro em exibição no momento.
   * @type {number}
   * @memberof ScreenContentComponent
   */
  frame: number;

  /**
   * Classes aplicadas ao container.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  containerClasses: string;

  /**
   * Estilos aplicados ao container.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  containerStyles: string;

  /**
   * Define se o usuário deve clicar no conteúdo para a interação finalizar (somente quando é apenas 1 frame).
   * @type {boolean}
   * @memberof ScreenImageComponent
   */
  userShouldClick: boolean;

  /**
   * Mensagem de orientação exibida para o clique.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  clickHint: string;


  /**
   * Arquivo utilizado como botão voltar.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  backButton: string;

  /**
   * Arquivo utilizado como botão avançar.
   * @type {string}
   * @memberof ScreenContentComponent
   */
  nextButton: string;

  /**
   *Creates an instance of ScreenContentComponent.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {ProtocolService} protocolService Serviços responsáveis pela comunicação com a plataforma LMS.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenContentComponent
   */
  constructor(private hintService: HintService, private protocolService: ProtocolService, private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenContentComponent
   */
  parseData(value: any) {
    let studentName = this.protocolService.getStudentName();

    if (typeof(value.content) === 'string') {
      this.content = [value.content.split('[[StudentName]]').join(studentName)];
    } else {
      this.content = [];

      for (let i = 0; i < value.content.length; i++) {
        this.content.push(value.content[i].split('[[StudentName]]').join(studentName));
      }
    }
    
    this.navHint = value.navHint || `Clique na seta`;
    this.frame = 0;
    this.containerClasses = value.containerClasses;
    this.containerStyles = value.containerStyles;

    this.userShouldClick = value.userShouldClick && this.content.length == 1;
    this.clickHint = value.clickHint || `Clique na imagem`;

    this.backButton = value.backButton || `./assets/images/screen_shared_nav_arrow_left.png`;
    this.nextButton = value.nextButton || `./assets/images/screen_shared_nav_arrow_right.png`;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenContentComponent
   */
  start() {
    if(this.content.length > 1) {
      this.hintService.show(this.navHint);
    } else if (!this.userShouldClick) {
      this.finish();
    } else {
      this.hintService.show(this.clickHint);
    }
  }


  /**
   * Conteúdo em exibição.
   * @readonly
   * @type {string}
   * @memberof ScreenContentComponent
   */
  get currentContent(): string {
    return this.frame >= 0 && this.frame < this.content.length ? this.content[this.frame] : null;
  }


  /**
   * Valida se o conteúdo possui navegação.
   * @returns {boolean}
   * @memberof ScreenContentComponent
   */
  hasNavigation(): boolean {
    return this.content.length > 1;
  }

  /**
   * Valida se é possível voltar na navegação.
   * @returns {boolean}
   * @memberof ScreenContentComponent
   */
  canGoBack(): boolean {
    return this.frame >= 0 && this.frame > 0;
  }

  /**
   * Valida se é possível avançar na navegação.
   * @returns {boolean}
   * @memberof ScreenContentComponent
   */
  canGoNext(): boolean {
    return this.frame >= 0 && this.frame < this.content.length - 1;
  }

  /**
   * Navega no conteúdo.
   * @param {boolean} goNext Define se a navegação está avançando.
   * @memberof ScreenContentComponent
   */
  navigate(goNext: boolean): void {
    if (this.frame < 0) {
      return;
    }

    if (!this.isFinished || this.hintService.currentMessage == this.DEFAULT_LINK_HINT_SINGLE || this.hintService.currentMessage == this.DEFAULT_LINK_HINT_MULTI) {
      this.hintService.hide();
    }

    let currentFrame = this.frame;
    this.frame = -1;

    this.timeoutService.set(() => {
      if (goNext) {
        this.frame = currentFrame + 1;
      } else {
        this.frame = currentFrame - 1;
      }

      this.timeoutService.set(() => {
        this.onNavigate();
      }, 300);
    }, 300);
  }

  /**
   * Realiza validações ao finalizar a navegação.
   * @memberof ScreenContentComponent
   */
  onNavigate(): void {
    if (!this.canGoNext()) {
      if (!this.container || this.container.nativeElement.getElementsByClassName('link').length == 0) {
        this.finish();
      }
    } else if (!this.isFinished) {
      this.hintService.show(this.navHint);
    }

    if (!this.container) {
      return;
    }

    let foundLinks = this.container.nativeElement.getElementsByClassName('link');
    if (foundLinks.length > 0) {
      this.hintService.show(foundLinks.length == 1 ? this.DEFAULT_LINK_HINT_SINGLE : this.DEFAULT_LINK_HINT_MULTI);

      for (let i = 0; i < foundLinks.length; i++) {
        const link = foundLinks[i];
        link.addEventListener('click', () => {
          if (link.innerHTML.indexOf('shared_icon_check.png') == -1) {
            link.classList.add('clicked');
  
            link.innerHTML += `<img style="position: absolute; top: -15px; left: 50%;" src="./assets/svgs/check.svg">`;
  
            let pendingLinks = this.container.nativeElement.querySelectorAll('.link:not(.clicked)');
            if (!pendingLinks.length && (this.hintService.currentMessage == this.DEFAULT_LINK_HINT_SINGLE || this.hintService.currentMessage == this.DEFAULT_LINK_HINT_MULTI)) {
              this.hintService.hide();

              if (!this.canGoNext()) {
                this.finish();
              }
            }
          }
        })
      }
    }
  }


  /**
   * Caso o recurso de clique esteja habilitado, finaliza o item quando o usuário clica sobre o mesmo.
   * @memberof ScreenContentComponent
   */
  onUserClick(): void {
    if (this.userShouldClick) {
      this.hintService.hide();

      this.userShouldClick = false;
      
      this.finish();
    }
  }
}
