import { Component } from '@angular/core';

import { fadeAnimation } from '../../animations/animations';
import { ScreenItem } from '../../base/screen-item.component';

@Component({
  selector: 'app-screen-custom-question-join-columns',
  templateUrl: './screen-custom-question-join-columns.component.html',
  styleUrls: ['./screen-custom-question-join-columns.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenCustomQuestionJoinColumnsComponent extends ScreenItem {
  /**
   * Opções exibidas para o exercício.
   * @type {string[]}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  options: string[];

  /**
   * Respostas exibidas para o exercício.
   * @type {string[]}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  answers: string[];

  /**
   * Ordem correta das respostas do exercício.
   * @type {number[]}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  ids: string[];

  /**
   * Respostas inseridas pelo usuário.
   * @type {number[]}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  userIds: string[];


  /**
   * Define se todas as opções foram selecionadas.
   * @type {boolean}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  allChecked: boolean;

  /**
   * Define se o exercício foi finalizado.
   * @type {boolean}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  checked: boolean;


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  parseData(value: any) {
    this.options = value.options;
    this.answers = value.answers;
    this.ids = value.ids;

    this.userIds = [];
    for (let i = 0; i < this.ids.length; i++) {
      this.userIds.push(``);
    }
  }


  onOptionChanged(element: HTMLInputElement, e: string, i: number): void {
    this.userIds[i] = e;

    if (isNaN(parseInt(this.userIds[i]))  || parseInt(this.userIds[i]) <= 0 || parseInt(this.userIds[i]) > this.answers.length) {
      element.value = ``;
      this.userIds[i] = ``;
    }
    
    this.allChecked = this.userIds.filter((v) => v == ``).length == 0;
  }


  /**
   * Valida se a resposta da posição recebida está correta.
   * @param {number} index
   * @returns {boolean}
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  isCorrect(index: number): boolean {
    return this.userIds[index] == this.ids[index];
  }


  /**
   * Finaliza o exercício mostrando as respostas corretas ao aluno.
   * @memberof ScreenCustomQuestionJoinColumnsComponent
   */
  check(): void {
    this.checked = true;

    this.finish();
  }
}
