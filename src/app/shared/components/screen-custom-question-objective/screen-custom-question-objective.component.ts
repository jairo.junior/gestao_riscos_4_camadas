import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { HintService } from '../../services/hint.service';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

/**
 * Componente responsável por renderizar questões objetivas.
 * @export
 * @class ScreenCustomQuestionObjectiveComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-question-objective',
  templateUrl: './screen-custom-question-objective.component.html',
  styleUrls: ['./screen-custom-question-objective.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenCustomQuestionObjectiveComponent extends ScreenItem {
  /**
   * Letras exibidas nas opções.
   * @type {string[]}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  readonly letters: string[] = [`A`, `B`, `C`, `D`, `E`, `F`];


  /**
   * Enunciado da questão.
   * @type {string}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  question: string;

  /**
   * Opções da questão.
   * @type {string[]}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  options: string[];

  /**
   * Tamanho da coluna para cada opção.
   * @type {number}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  optionsColSize: number;

  /**
   * Posição da opção correta na lista.
   * @type {number}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  correct: number;

  /**
   * Feedbacks exibidos para as opções.
   * @type {string[]}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  feedbacks: string[];


  /**
   * Define se a interação já foi inicializada.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  private _started: boolean;

  /**
   * Posição da opção selecionada.
   * @private
   * @type {number}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  private _selected: number;

  /**
   * Define se o feedback deve ser exibido.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  private _showFeedback: boolean;


  /**
   * Construtor.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenImageComponent
   */
  constructor(private hintService: HintService, private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Valida se a opção deve ficar desabilitada.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  isOptionDisabled(): boolean {
    return !this._started || this._selected >= 0;
  }

  /**
   * Valida se a opção está selecionada.
   * @param {number} index Posição da opção.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  isOptionSelected(index: number): boolean {
    return this._selected == index;
  }

  /**
   * Define se uma opção foi selecionada.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  get isSelected(): boolean {
    return this._selected >= 0;
  }

  /**
   * Define se o usuário acertou a questão.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  get isCorrect(): boolean {
    return this._selected == this.correct;
  }

  /**
   * Feedback exibido para a questão.
   * @returns {string}
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  get feedback(): string {
    return this._showFeedback ? this.feedbacks[this._selected] : null;
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenImageComponent
   */
  parseData(value: any) {
    this.question = value.question;
    this.options = value.options;
    this.correct = value.correct;
    this.feedbacks = value.feedbacks;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenContentComponent
   */
  start() {
    this._started = true;

    this.hintService.show(`Clique em uma das opções`);
  }


  /**
   * Realiza a seleção da opção.
   * @param {number} index Posição da opção selecionada.
   * @memberof ScreenCustomQuestionObjectiveComponent
   */
  trySelectOption(index: number): void {
    if (this._selected >= 0) {
      return;
    }

    this._selected = index;

    this.hintService.hide();

    this.timeoutService.set(() => {
      this._showFeedback = true;

      this.finish();
    }, 500);
  }
}
