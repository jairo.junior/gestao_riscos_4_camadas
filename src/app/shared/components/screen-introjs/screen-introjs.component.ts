import { Component } from '@angular/core';
import { IntroJs } from 'intro.js';
import * as introJs from 'intro.js/intro.js';

import { fadeAnimation } from '../../animations/animations';
import { ScreenItem } from '../../base/screen-item.component';

/**
 * Componente responsável por renderizar botão que inicia interação da biblioteca Intro.js.
 * @export
 * @class ScreenIntrojsComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-introjs',
  templateUrl: './screen-introjs.component.html',
  styleUrls: ['./screen-introjs.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenIntrojsComponent extends ScreenItem {
  /**
   * Construtor.
   * @memberof ScreenIntrojsComponent
   */
  constructor() {
    super();
  }

  /**
   * Inicia a execução do item.
   * @memberof ScreenIntrojsComponent
   */
  start() {
    
  }

  /**
   * Dispara a abertura do tutorial e finaliza a interação.
   * @memberof ScreenIntrojsComponent
   */
  onUserClick() {
    let tour: IntroJs = introJs();

    tour.setOption("prevLabel", "Voltar");
    tour.setOption("nextLabel", "Prosseguir");
    tour.setOption("skipLabel", "Fechar");
    tour.setOption("doneLabel", "Fechar");
    tour.setOption("showStepNumbers", false);
    tour.start();

    this.finish();
  }
}
