import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { HintService } from '../../services/hint.service';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

/**
 * Interface que define opções da questão.
 * @interface IOption
 */
interface IOption { text: string, correct: boolean };

/**
 * Componente responsável por renderizar questões objetivas.
 * @export
 * @class ScreenCustomQuestionTrueOrFalseComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-question-true-or-false',
  templateUrl: './screen-custom-question-true-or-false.component.html',
  styleUrls: ['./screen-custom-question-true-or-false.component.scss'],
  animations: [fadeAnimation]
})
export class ScreenCustomQuestionTrueOrFalseComponent extends ScreenItem {
  /**
   * Letras exibidas nas opções.
   * @type {string[]}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  readonly letters: string[] = [`A`, `B`, `C`, `D`, `E`, `F`];

  /**
   * Cabeçalho da tablela de questões.
   * @type {string}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  header: { firstColumn: string, secondColumn: string, thirdColumn: string };


  /**
   * Enunciado da questão.
   * @type {string}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  question: string;

  /**
   * Opções da questão.
   * @type {string[]}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  options: IOption[];


  /**
   * Define se o botão conferir deve ser exibido.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  public check: boolean;

  /**
   * Define se o exercício já foi finalizado.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  public isFinish: boolean;

  /**
   * Posição da opção selecionada.
   * @public
   * @type {Array}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  public selected: boolean[] = [];


  /**
   * Verifica questões correras/incorretas.
   * @public
   * @type {Array}
   * @memberof ScreenCustomQuestionTrueOrFalseComponent
   */
  public feedback: boolean[] = [];

  /**
   * Construtor.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenImageComponent
   */
  constructor(private hintService: HintService, private timeoutService: TimeoutService) {
    super();
  }

  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenImageComponent
   */
  parseData(value: any) {
    this.header = value.header;
    this.question = value.question;
    this.options = value.options;

    for (let i = 0; i < this.options.length; i++) {
      this.selected.push(null);
    }
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenContentComponent
   */
  start() {
    this.isFinish = false;
    this.check = false;

    this.hintService.show(`Clique em uma das colunas`);
  }

  /**
   * Marcar opção clicada.
   * @param {number} index Valor da questão.
   * @param {boolean} option valor da opção marcada.
   * @memberof ScreenImageComponent
   */
  clickOption(index: number, option: boolean) {
    if (this.isFinish) return;

    this.selected[index] = option;

    for (let i = 0; i < this.selected.length; i++) {
      if (this.selected[i] == null) {
        return;
      }
    }

    this.check = true;
    this.hintService.currentMessage = "Clique no botão Conferir";
  }

  /**
   * Confere respostas marcadas.
   * @memberof ScreenImageComponent
   */
  checkOptions() {
    this.isFinish = true;

    this.check = false;
    this.hintService.hide();

    for (let i = 0; i < this.selected.length; i++) {
      this.feedback[i] = this.selected[i] == this.options[i].correct;
    }

    console.log(this.feedback)

    this.timeoutService.set(() => {
      this.finish();
    }, 500);

  }
}

