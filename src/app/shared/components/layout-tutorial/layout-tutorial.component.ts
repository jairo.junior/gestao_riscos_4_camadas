import { Component, EventEmitter, Output } from '@angular/core';

import { fadeAnimation } from './../../animations/animations';

@Component({
  selector: 'app-layout-tutorial',
  templateUrl: './layout-tutorial.component.html',
  styleUrls: ['./layout-tutorial.component.scss'],
  animations: [ fadeAnimation ]
})
export class LayoutTutorialComponent {
  /**
   * Emissor do evento que define que o componente foi fechado.
   * @type {EventEmitter}
   * @memberof LayoutTutorialComponent
   */
  @Output()
  close: EventEmitter<object>;


  /**
   * Construtor.
   * @memberof LayoutTutorialComponent
   */
  constructor() {
    this.close = new EventEmitter();
  }


  /**
   * Dispara evento de fechamento do componente.
   * @memberof LayoutTutorialComponent
   */
  dispatchClose(): void {
    this.close.emit({});
  }
}
