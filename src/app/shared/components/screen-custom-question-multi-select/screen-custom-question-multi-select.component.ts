import { Component } from '@angular/core';

import { fadeAnimation } from '../../animations/animations';
import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';

/**
 * Interface que define opções da questão.
 * @interface IOption
 */
interface IOption { text: string, isCorrect: boolean };

/**
 * Componente responsável por renderizar questão de seleção múltipla.
 * @export
 * @class ScreenCustomQuestionMultiSelectComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-question-multi-select',
  templateUrl: './screen-custom-question-multi-select.component.html',
  styleUrls: ['./screen-custom-question-multi-select.component.scss'],
  animations: [fadeAnimation]
})
export class ScreenCustomQuestionMultiSelectComponent extends ScreenItem {
  /**
   * Lista de opções renderizadas.
   * @type {IOption[]}
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  options: IOption[];

  /**
   * Feedback exibido para o exercício.
   * @type {string}
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  feedback: string;

  /**
   * Define quais opções estão selecionadas.
   * @type {boolean[]}
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  selected: boolean[];

  /**
   * Define cor da opção se ela está correta ou errada.
   * @type {string[]}
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  colorOption: string[] = [];

  /**
   * Define se o usuário conferiu as respostas.
   * @type {boolean}
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  checked: boolean;


  /**
   * Construtor.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenCustomExamComponent
   */
  constructor(private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  parseData(value: any) {
    this.options = value.options;
    this.feedback = value.feedback;

    this.selected = [];
    for (let i = 0; i < this.options.length; i++) {
      this.selected.push(false);
    }
  }


  /**
   * Altera a seleção da opção na posição recebida.
   * @param {number} i Posição que será modificada.
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  toggleSelection(i: number): void {
    if (this.isFinished) return;

    this.selected[i] = !this.selected[i];
  }

  /**
   * Valida se o item da posição recebida está selecionado.
   * @param {number} i Posição que será validada.
   * @returns {boolean} Resultado da validação.
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  isSelected(i: number): boolean {
    return this.selected[i];
  }

  /**
   * Marca o exercício como finalizado.
   *
   * @memberof ScreenCustomQuestionMultiSelectComponent
   */
  check(): void {
    this.checked = true;

    for (let i = 0; i < this.options.length; i++) {
      this.colorOption.push(this.options[i].isCorrect ? 'success' : 'error');

    }

    this.finish();
  }
}
