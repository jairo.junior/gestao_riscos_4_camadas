import { Component, ComponentFactory, ComponentFactoryResolver, OnInit, Renderer2, ViewChild } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { ViewHostDirective } from '../../directives/view-host.directive';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';
import { AudioService } from './../../services/audio.service';
import { HintService } from './../../services/hint.service';
import { StructureService } from './../../services/structure.service';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável pela construção e execução das telas.
 * @export
 * @class ScreenComponent
 */
export class ScreenComponent implements OnInit {
  /**
   * Elemento responsável por renderizar os componentes em tempo de execução.
   * @type {ViewHostDirective}
   * @memberof ScreenComponent
   */
  @ViewChild(ViewHostDirective)
  appViewHost: ViewHostDirective;


  /**
   * Posição do item em execução na lista.
   * @private
   * @type {number}
   * @memberof ScreenComponent
   */
  private _runningItemIndex: number;

  /**
   * Factories para os componentes das telas.
   * @private
   * @type {Array<ComponentFactory<ScreenItem>>}
   * @memberof ScreenComponent
   */
  private _itemsFactories: Array<ComponentFactory<ScreenItem>>;


  /**
   * Construtor.
   * @param {ComponentFactoryResolver} componentFactoryResolver Factory responsável pela criação de instâncias de componentes.
   * @param {Renderer2} renderer2 Utilitário para trabalhar com o DOM dos componentes.
   * @param {AudioService} audioService Serviços responsáveis pela reprodução de áudios.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {StructureService} structureService Serviços responsáveis por disponibilizar a estrutura do curso.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenComponent
   */
  constructor(private componentFactoryResolver: ComponentFactoryResolver, private renderer2: Renderer2,
              private audioService: AudioService, private hintService: HintService, private structureService: StructureService,
              private timeoutService: TimeoutService) {
    structureService.screenChanged.subscribe((e) => this.onScreenChanged(e));
  }

  ngOnInit(): void {
    this.buildScreenContent();
  }

  /**
   * Realiza a troca de tela quando o evento é disparado.
   * @private
   * @param {*} e Informações do evento.
   * @memberof ScreenComponent
   */
  private onScreenChanged(e: any) {
    this.buildScreenContent();
  }

  /**
   * Constrói o conteúdo da tela e em seguida inicia a execução.
   * @private
   * @memberof ScreenComponent
   */
  private buildScreenContent() {
    this.hintService.hide();
    this.timeoutService.clearAll();

    const viewContainer = this.appViewHost.viewContainerRef;
    viewContainer.clear();

    this.timeoutService.set(() => {
      this.audioService.destroy();
      this.hintService.hide();

      this._runningItemIndex = -1;
      this._itemsFactories = [];

      if (this.structureService.screen == null || this.structureService.screen.items == null)
        return;

      for (let index = 0; index < this.structureService.screen.items.length; index++) {
        const itemData = this.structureService.screen.items[index];
        this._itemsFactories.push(this.componentFactoryResolver.resolveComponentFactory(itemData.type));
      }

      this.buildAndRunItem();
    }, 500);
  }

  /**
   * Constrói a referência do componente e executa o mesmo.
   * @private
   * @memberof ScreenComponent
   */
  private buildAndRunItem() {
    this._runningItemIndex++;

    const viewContainer = this.appViewHost.viewContainerRef;
    const itemData = this.structureService.screen.items[this._runningItemIndex];
    const itemRef = viewContainer.createComponent(this._itemsFactories[this._runningItemIndex]);

    if (itemData.classes) {
      let splittedClasses: string[] = itemData.classes.split(" ");
      for (let i = 0; i < splittedClasses.length; i++) {
        this.renderer2.addClass(itemRef.location.nativeElement, splittedClasses[i]);
      }
    }

    if (itemData.styles) {
      let splittedStyles: string[] = itemData.styles.split(";");
      for (let i = 0; i < splittedStyles.length; i++) {
        if (splittedStyles[i]) {
          let brokenStyle: string[] = splittedStyles[i].split(":");

          this.renderer2.setStyle(itemRef.location.nativeElement, brokenStyle[0].trim(), brokenStyle[1].trim());
        }
      }
    }

    itemRef.instance.data = itemData.data;
    itemRef.instance.finished.subscribe((e) => this.onItemFinished(e));
    itemRef.instance.start();
  }

  /**
   * Finaliza a execução do item.
   * @private
   * @param {*} e Informações do evento.
   * @memberof ScreenComponent
   */
  private onItemFinished(e: any) {
    let action: (...args: any[]) => void = () => {
      if (this._runningItemIndex === this.structureService.screen.items.length - 1) {
        if (!this.structureService.screen.completed) {
          const message = this.structureService.screenCount < this.structureService.totalScreensCount ? 'Clique em Prosseguir'
                                                                                                      : 'Você finalizou o curso.';
  
          this.hintService.show(message);
        }
  
        this.structureService.finishCurrentScreen();
  
        if (e.advance) {
          this.structureService.tryGoNext();
        }
      } else {
        this.timeoutService.set(() => this.buildAndRunItem(), 200);
      }
    };

    const itemData = this.structureService.screen.items[this._runningItemIndex];
    if (itemData.hideCurrentAndPreviousOnFinishCount != null && itemData.hideCurrentAndPreviousOnFinishCount > 0) {
      const viewContainer = this.appViewHost.viewContainerRef;
      for (let i = 0; i < itemData.hideCurrentAndPreviousOnFinishCount; i++) {
        viewContainer.remove(viewContainer.length - 1);
      }

      this.timeoutService.set(action, 250);
    } else {
      action();
    }
  }
}
