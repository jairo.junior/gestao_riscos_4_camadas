import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

/**
 * Componente responsável por renderizar questões objetivas.
 * @export
 * @class ScreenCustomQuestionInputComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-question-input',
  templateUrl: './screen-custom-question-input.component.html',
  styleUrls: ['./screen-custom-question-input.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenCustomQuestionInputComponent extends ScreenItem {
  /**
   * Enunciado da questão.
   * @type {string}
   * @memberof ScreenCustomQuestionInputComponent
   */
  question: string;

  /**
   * Enunciado exibido ao lado das opções.
   * @type {string}
   * @memberof ScreenCustomQuestionInputComponent
   */
  sideQuestion: string;

  /**
   * Opções.
   * @type {string[]}
   * @memberof ScreenCustomQuestionInputComponent
   */
  options: string[];

  /**
   * Posição das opções corretas nas listas.
   * @type {string[]}
   * @memberof ScreenCustomQuestionInputComponent
   */
  corrects: string[];

  /**
   * Posição da opção selecionada.
   * @private
   * @type {string[]}
   * @memberof ScreenCustomQuestionInputComponent
   */
  selected: { value: string}[];

  /**
   * Feedback exibidos ao término.
   * @type {string}
   * @memberof ScreenCustomQuestionInputComponent
   */
  feedback: string;

  /**
   * Feedback para quando o usuário não acerta todas as opções.
   * @type {string}
   * @memberof ScreenCustomQuestionInputComponent
   */
  wrongFeedback: string;


  /**
   * Define se a interação já foi inicializada.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  private _started: boolean;

  /**
   * Define se o usuário já mandou checar as respostas.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  private _isChecked: boolean;

  /**
   * Define se o feedback deve ser exibido.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  private _showFeedback: boolean;


  /**
   * Construtor.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenImageComponent
   */
  constructor(private timeoutService: TimeoutService) {
    super();

    this.selected = [];
  }


  /**
   * Define se as opções devem estar desabilitadas.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get optionsDisabled(): boolean {
    return !this._started || this._isChecked;
  }

  /**
   * Define se o usuário já conferiu a resposta correta.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get checked(): boolean {
    return this._isChecked;
  }

  /**
   * Valida se o usuário já selecionou todas as opções e pode conferir.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get canCheck(): boolean {
    return this.selected.length == this.options.length && this.allFilled;
  }

  /**
   * Valida se todos os campos estão preenchidos.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get allFilled(): boolean {
    for (let i = 0; i < this.selected.length; i++) {
      if (this.selected[i].value == ``) {
        return false;
      }
    }
    return true;
  }

  /**
   * Define se o usuário selecionou a questão.
   * @param {number} index Posição da questão validada.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  isChecked(index: number): boolean {
    return !!this.selected[index];
  }

  /**
   * Define se o usuário acertou a questão.
   * @param {number} index Posição da questão validada.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  isCorrect(index: number): boolean {
    return this.selected[index].value.toLowerCase() == this.corrects[index].toLowerCase();
  }

  /**
   * Valida se todas as opções estão corretas.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get allCorrect(): boolean {
    for (let i = 0; i < this.options.length; i++) {
      if (!this.isCorrect(i)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Define se o feedback está visível.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionInputComponent
   */
  get feedbackVisible(): boolean {
    return this._showFeedback;
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenImageComponent
   */
  parseData(value: any) {
    this.question = value.question;
    this.sideQuestion = value.sideQuestion;
    this.options = value.options;
    this.corrects = value.corrects;
    this.feedback = value.feedback;
    this.wrongFeedback = value.wrongFeedback;

    for (let i = 0; i < this.options.length; i++) {
      this.selected.push({ value: `` });
    }
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenContentComponent
   */
  start() {
    this._started = true;
  }


  /**
   * Realiza a seleção da opção.
   * @param {number} index Posição da questão.
   * @param {string} value Valor da opção selecionada.
   * @memberof ScreenCustomQuestionInputComponent
   */
  tryInputOption(index: number, value: string): void {
    console.log(index, value);
    if (this._isChecked) {
      return;
    }

    this.selected[index].value = value;
  }

  /**
   * Chega as respostas corretas e finaliza a interação.
   * @returns {void}
   * @memberof ScreenCustomQuestionInputComponent
   */
  tryCheckAnswers(): void {
    if (this._isChecked) {
      return;
    }

    this._isChecked = true;
    
    this.timeoutService.set(() => {
      this._showFeedback = true;

      this.finish();
    }, 500);
  }
}
