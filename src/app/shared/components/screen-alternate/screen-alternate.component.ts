import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { HintService } from '../../services/hint.service';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

@Component({
  selector: 'app-screen-alternate',
  templateUrl: './screen-alternate.component.html',
  styleUrls: ['./screen-alternate.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por renderizar itens clicáveis alternados.
 * @export
 * @class ScreenAlternateComponent
 * @extends {ScreenItem}
 */
export class ScreenAlternateComponent extends ScreenItem {
  /**
   * Posição padrão para as checks de conclusão.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  readonly DEFAULT_CHECK_POSITION: string = `top right`;


  /**
   * Posição do item em exibição.
   * @private
   * @type {number}
   * @memberof ScreenAlternateComponent
   */
  private _currentIndex: number;


  /**
   * Título do conteúdo exibido.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  currentTitle: string;

  /**
   * Texto do conteúdo exibido.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  currentText: string;

  /**
   * Quadro em exibição no momento.
   * @type {number}
   * @memberof ScreenAlternateComponent
   */
  frame: number;

  /**
   * Define se está ocorrendo navegação.
   * @type {boolean}
   * @memberof ScreenAlternateComponent
   */
  navigating: boolean;

  /**
   * Define se o popup pode ser fechado.
   * @type {boolean}
   * @memberof ScreenAlternateComponent
   */
  interactionFinished: boolean;


  /**
   * Define se os botões usam estrutura de container HTML.
   *
   * @type {boolean}
   * @memberof ScreenAlternateComponent
   */
  useContainer: boolean;

  /**
   * Tamanho aplicado as linhas do container.
   *
   * @type {number}
   * @memberof ScreenAlternateComponent
   */
  containerSize: number;

  /**
   * Offset aplicado ao container.
   * @type {number}
   * @memberof ScreenAlternateComponent
   */
  containerOffset: number;

  /**
   * Classes aplicadas ao container da caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  contentAreaClasses: string;

  /**
   * Conteúdo inserido antes de iniciar interação.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  preContent: string

  /**
   * Estilos aplicados ao container da caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  contentAreaStyles: string;

  /**
   * Classes aplicadas ao container da caixa inicial.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  preContentAreaClasses: string;

  /**
   * Estilos aplicados ao container da caixa inicial.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  preContentAreaStyles: string;

  /**
   * Classes aplicadas a caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  contentBoxClasses: string;

  /**
   * Estilos aplicados a caixa que exibe o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  contentBoxStyles: string;

  /**
   * Posição onde será exibido o check de conclusão do item.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  checkPosition: string;

  /**
   * Estilos CSS aplicados aos marcadores separadamente.
   * @type {Array<string>}
   * @memberof ScreenAlternateComponent
   */
  checkStyles: Array<string>;

  /**
   * Mensagem de orientação customizada.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  customHint: string;

  /**
   * Mensagem de orientação customizada utilizada na navegação.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  navCustomHint: string;

  /**
   * Textos dos botões exibidos.
   * @type {Array<string>}
   * @memberof ScreenAlternateComponent
   */
  buttons: Array<string>;

  /**
   * Classes aplicadas aos botões para centralizar o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  buttonsClasses: string[];

  /**
   * Classes aplicadas aos botões para centralizar o conteúdo.
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  buttonsAlign: string;

  /**
   * Conteúdo dos botões.
   * @type {Array<string>}
   * @memberof ScreenAlternateComponent
   */
  contents: Array<string | string[]>;

  /**
   * Classes aplicadas ao botão fechar para cada conteúdo.
   * @type {string[]}
   * @memberof ScreenAlternateComponent
   */
  closeButtonClasses: string[];

  /**
   * Status de visualização de cada item.
   * @type {Array<boolean>}
   * @memberof ScreenAlternateComponent
   */
  status: Array<boolean>;


  /**
   * Define se só aceita a seleção de um item.
   * @type {boolean}
   * @memberof ScreenAlternateComponent
   */
  singleSelection: boolean;


  /**
   * Construtor.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenAlternateComponent
   */
  constructor(public hintService: HintService, private timeoutService: TimeoutService) {
    super();
  }


  /**
   * Classes aplicadas ao botão fechar.
   * @readonly
   * @type {string}
   * @memberof ScreenAlternateComponent
   */
  get currentCloseButtonClasses(): string {
    return this._currentIndex >= 0 ? this.closeButtonClasses[this._currentIndex] : '';
  }



  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenAlternateComponent
   */
  parseData(value: any) {
    this.useContainer = value.useContainer;
    this.containerSize = value.containerSize;
    this.containerOffset = value.containerOffset;
    this.contentAreaClasses = value.contentAreaClasses;
    this.contentAreaStyles = value.contentAreaStyles;
    this.preContentAreaClasses = value.preContentAreaClasses;
    this.preContentAreaStyles = value.preContentAreaStyles;
    this.contentBoxClasses = value.contentBoxClasses;
    this.contentBoxStyles = value.contentBoxStyles;
    this.checkPosition = value.checkPosition || this.DEFAULT_CHECK_POSITION;
    this.checkStyles = value.checkStyles;
    this.customHint = value.customHint;
    this.navCustomHint = value.navCustomHint;
    this.buttons = value.buttons;
    this.buttonsClasses = value.buttonsClasses;
    this.buttonsAlign = value.buttonsAlign;
    this.preContent = value.preContent;
    this.contents = value.contents;
    this.singleSelection = value.singleSelection;

    this.closeButtonClasses = this.parseListData(value.closeButtonClasses, this.buttons.length);

    if (this.checkStyles === null || this.checkStyles === undefined) {
      this.checkStyles = new Array<string>();
    }

    while (this.checkStyles.length < this.buttons.length) {
      this.checkStyles.push('');
    }

    if (this.buttonsClasses === null || this.buttonsClasses === undefined) {
      this.buttonsClasses = new Array<string>();
    }

    while (this.buttonsClasses.length < this.buttons.length) {
      this.buttonsClasses.push('');
    }
  }

  /**
   * Gera uma lista da propriedade recebida para atender a todos os popups.
   * @private
   * @param {*} value Valor que será lido.
   * @param {number} count Quantidade de posição que a lista deverá possuir.
   * @returns {string[]} Lista gerada.
   * @memberof ScreenAlternateComponent
   */
  private parseListData(value: any, count: number): string[] {
    let result: string[] = [];

    if (Array.isArray(value)) {
      result = value;

      while (result.length < count) {
        result.push('');
      }
    } else  {
      for (let i = 0; i < count; i++) {
        result.push(value + '' || '');
      }
    }

    return result;
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenAlternateComponent
   */
  start() {
    this.status = new Array<boolean>();

    for (let i = 0; i < this.buttons.length; i++) {
      this.status.push(false);
    }

    this.showHintMessage();
  }


  /**
   * Oculta o conteúdo atual e prepara para a exibição do contéudo recebido.
   * @param {number} index Posição do item clicado na lista.
   * @memberof ScreenAlternateComponent
   */
  view(index: number) {
    this.hideHintMessage();

    this.preContent = null;

    this.currentTitle = null;
    this.currentText = null;

    this.timeoutService.set(() => {
      this.showContent(index);
    }, 500);
  }

  /**
   * Exibe o contéudo da posição recebida.
   * @private
   * @param {number} index Posição do item que será exibido.
   * @memberof ScreenAlternateComponent
   */
  private showContent(index: number): void {
    this._currentIndex = index;

    this.frame = 0;
    this.currentTitle = this.buttons[index];
    this.currentText = this.hasNavigation ? this.contents[index][this.frame].toString() : this.contents[index].toString();
    this.status[index] = true;

    if (this.singleSelection) {
      for (let i = 0; i < this.status.length; i++) {
        this.status[i] = true;
      }
    }

    if (!this.hasNavigation) {
      if (this.status.indexOf(false) === -1) {
        this.finish();
      } else {
        this.showHintMessage();
      }

      this.interactionFinished = true;
    } else {
      if (!this.isFinished) {
        this.showNavHintMessage();
      }
    }
  }

  /**
   * Valida se o item da posição recebida deve ser marcado com o check.
   * @param {number} index Posição da lista que será validada.
   * @memberof ScreenAlternateComponent
   */
  isChecked(index: number) {
    return this.status[index] === true;
  }

  /**
   * Valida se o item é o que está selecionado no momento.
   * @param {number} index
   * @returns
   * @memberof ScreenAlternateComponent
   */
  isCurrent(index: number) {
    return index == this._currentIndex;
  }


  /**
   * Valida se o conteúdo possui navegação.
   * @returns {boolean}
   * @memberof ScreenAlternateComponent
   */
  get hasNavigation(): boolean {
    return this._currentIndex >= 0 ? Array.isArray(this.contents[this._currentIndex]) : false;
  }

  /**
   * Valida se é possível voltar na navegação.
   * @returns {boolean}
   * @memberof ScreenAlternateComponent
   */
  canGoBack(): boolean {
    return this.frame >= 0 && this.frame > 0;
  }

  /**
   * Valida se é possível avançar na navegação.
   * @returns {boolean}
   * @memberof ScreenAlternateComponent
   */
  canGoNext(): boolean {
    return this.frame >= 0 && this.frame < this.contents[this._currentIndex].length - 1;
  }

  /**
   * Navega no conteúdo.
   * @param {boolean} goNext Define se a navegação está avançando.
   * @memberof ScreenAlternateComponent
   */
  navigate(goNext: boolean): void {
    if (this.frame < 0) {
      return;
    }

    this.navigating = true;
    this.frame = this.frame + (goNext ? +1 : -1);
    this.currentText = null;

    this.hideHintMessage();

    this.timeoutService.set(() => {
      this.navigating = false;
      this.currentText = this.contents[this._currentIndex][this.frame].toString();
  
      if (!this.canGoNext()) {
        this.interactionFinished = true;

        if (this.status.indexOf(false) === -1) {
          this.finish();
        }

        this.showHintMessage();
      } else {
        this.showNavHintMessage();
      }
    }, 300);
  }


  /**
   * Exibe a mensagem de orientação para abertura dos popups.
   * @private
   * @memberof ScreenAlternateComponent
   */
  private showHintMessage() {
    if (!this.isFinished) {
      const hasCustomMessage = this.customHint !== null && this.customHint !== undefined && this.customHint.length > 0;
      this.hintService.show(hasCustomMessage ? this.customHint : 'Clique sobre os botões');
    }
  }

  /**
   * Exibe a mensagem de orientação de navegação.
   * @private
   * @memberof ScreenAlternateComponent
   */
  private showNavHintMessage() {
    if (!this.isFinished) {
      const hasCustomMessage = this.navCustomHint !== null && this.navCustomHint !== undefined && this.navCustomHint.length > 0;
      this.hintService.show(hasCustomMessage ? this.navCustomHint : 'Clique na seta');
    }
  }

  /**
   * Oculta a mensagem de orientação.
   * @private
   * @memberof ScreenAlternateComponent
   */
  private hideHintMessage() {
    if (!this.isFinished) {
      this.hintService.hide();
    }
  }
}
