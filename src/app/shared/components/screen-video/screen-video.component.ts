import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { HintService } from '../../services/hint.service';
import { fadeAnimation } from './../../animations/animations';

@Component({
  selector: 'app-screen-video',
  templateUrl: './screen-video.component.html',
  styleUrls: ['./screen-video.component.scss'],
  animations: [ fadeAnimation ]
})

/**
 * Componente responsável por renderizar o player de vídeos.
 * @export
 * @class ScreenVideoComponent
 * @extends {ScreenItem}
 */
export class ScreenVideoComponent extends ScreenItem {
  /**
   * Imagem utilizada como poster do vídeo.
   * @type {string}
   * @memberof ScreenVideoComponent
   */
  poster: string;

  /**
   * Vídeo exibido.
   * @type {string}
   * @memberof ScreenVideoComponent
   */
  source: string;

  /**
   * Estilos aplicados ao componente de video.
   * @type {string}
   * @memberof ScreenVideoComponent
   */
  styles: string;

  /**
   * Construtor.
   * @memberof ScreenVideoComponent
   */
  constructor(private hintService: HintService) {
    super();
  }

  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenVideoComponent
   */
  parseData(value: any) {
    this.poster = value.poster !== null && value.poster !== undefined && value.poster.length > 0
                        ? value.poster : './assets/images/comp_video_placeholder.jpg';
    this.source = value.source;
    this.styles = value.styles;

    this.hintService.show("Clique no play");
  }

  playing() {
    this.hintService.hide();
  }
}
