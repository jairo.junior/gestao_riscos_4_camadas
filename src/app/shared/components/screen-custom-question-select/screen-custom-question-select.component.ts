import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';

/**
 * Componente responsável por renderizar questões objetivas.
 * @export
 * @class ScreenCustomQuestionSelectComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-question-select',
  templateUrl: './screen-custom-question-select.component.html',
  styleUrls: ['./screen-custom-question-select.component.scss'],
  animations: [ fadeAnimation ]
})
export class ScreenCustomQuestionSelectComponent extends ScreenItem {
  /**
   * Enunciado principal para todos os itens.
   * @type {string}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  mainQuestion: string;

  /**
   * Enunciados das questõeo.
   * @type {string[]}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  questions: string[];

  /**
   * Opções das questõeo.
   * @type {string[][]}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  options: string[][];

  /**
   * Posição das opções corretas nas listas.
   * @type {number[]}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  corrects: number[];


  /**
   * Define se as combos serão inseridas no meio do texto.
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  inlineStyle: boolean;

  /**
   * Estilos aplicados na combo do modo em linha.
   * @type {string}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  inlineStyleContainerStyles: string;

  /**
   * Texto exibido após a última combo.x
   * @type {string}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  afterText: string;


  /**
   * Feedback exibidos ao término.
   * @type {string}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  feedback: string;

  /**
   * Feedback para quando o usuário não acerta todas as opções.
   * @type {string}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  wrongFeedback: string;


  /**
   * Define se a interação já foi inicializada.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  private _started: boolean;

  /**
   * Posição da opção selecionada.
   * @private
   * @type {number}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  private _selected: number[];

  /**
   * Define se o usuário já mandou checar as respostas.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  private _isChecked: boolean;

  /**
   * Define se o feedback deve ser exibido.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  private _showFeedback: boolean;


  /**
   * Construtor.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenImageComponent
   */
  constructor(private timeoutService: TimeoutService) {
    super();

    this._selected = [];
  }


  /**
   * Define se as opções devem estar desabilitadas.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  get optionsDisabled(): boolean {
    return !this._started || this._isChecked;
  }

  /**
   * Define se o usuário já conferiu a resposta correta.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  get checked(): boolean {
    return this._isChecked;
  }

  /**
   * Valida se o usuário já selecionou todas as opções e pode conferir.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  get canCheck(): boolean {
    return this._selected.length == this.questions.length && this._selected.indexOf(-1) == -1;
  }

  /**
   * Define se o usuário selecionou a questão.
   * @param {number} index Posição da questão validada.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  isChecked(index: number): boolean {
    return this._selected[index] >= 0
  }

  /**
   * Define se o usuário acertou a questão.
   * @param {number} index Posição da questão validada.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  isCorrect(index: number): boolean {
    return this._selected[index] == this.corrects[index];
  }

  /**
   * Valida se todas as opções estão corretas.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  get allCorrect(): boolean {
    for (let i = 0; i < this.questions.length; i++) {
      if (!this.isCorrect(i)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Define se o feedback está visível.
   * @returns {boolean}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  get feedbackVisible(): boolean {
    return this._showFeedback;
  }


  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenImageComponent
   */
  parseData(value: any) {
    this.mainQuestion = value.mainQuestion;
    this.questions = value.questions;
    this.options = value.options;
    this.corrects = value.corrects;
    this.inlineStyle = value.inlineStyle;
    this.inlineStyleContainerStyles = value.inlineStyleContainerStyles;
    this.afterText = value.afterText;
    this.feedback = value.feedback;
    this.wrongFeedback = value.wrongFeedback;

    for (let i = 0; i < this.questions.length; i++) {
      this._selected.push(-1);
    }
  }


  /**
   * Inicia a execução do item.
   * @memberof ScreenContentComponent
   */
  start() {
    this._started = true;
  }


  /**
   * Realiza a seleção da opção.
   * @param {number} index Posição da questão.
   * @param {string} value Valor da opção selecionada.
   * @memberof ScreenCustomQuestionSelectComponent
   */
  trySelectOption(index: number, value: string): void {
    if (this._isChecked) {
      return;
    }

    this._selected[index] = this.options[index].indexOf(value);
  }

  /**
   * Chega as respostas corretas e finaliza a interação.
   * @returns {void}
   * @memberof ScreenCustomQuestionSelectComponent
   */
  tryCheckAnswers(): void {
    if (this._isChecked) {
      return;
    }

    this._isChecked = true;
    
    this.timeoutService.set(() => {
      this._showFeedback = true;

      this.finish();
    }, 500);
  }
}
