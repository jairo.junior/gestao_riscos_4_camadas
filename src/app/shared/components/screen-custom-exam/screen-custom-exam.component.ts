import { Component } from '@angular/core';

import { ScreenItem } from '../../base/screen-item.component';
import { TimeoutService } from '../../services/timeout.service';
import { fadeAnimation } from './../../animations/animations';
import { environment } from 'src/environments/environment';
import { ProtocolService } from '../../services/protocol.service';
import { HintService } from '../../services/hint.service';

/**
 * Interface que define estrutura de uma questão.
 * @interface IQuestion
 */
interface IQuestion { guid: string, text: string, options: string[], optionsFeedbacks?: IQuestionFeedback[], correct: number }

/**
 * Interface que define um feedback de opções.
 * @interface IQuestionFeedback
 */
interface IQuestionFeedback { text: string, containerClasses: string }

/**
 * Interface que define estrutura dos feedbacks.
 * @interface IFeedback
 */
interface IFeedback { text: string, containerClasses: string, minScore: number, maxScore: number, canTryAgain: boolean }

/**
 * Componente responsável por renderizar avaliações nas telas.
 * @export
 * @class ScreenCustomExamComponent
 * @extends {ScreenItem}
 */
@Component({
  selector: 'app-screen-custom-exam',
  templateUrl: './screen-custom-exam.component.html',
  styleUrls: ['./screen-custom-exam.component.scss'],
  animations: [fadeAnimation]
})
export class ScreenCustomExamComponent extends ScreenItem {
  /**
   * Letras utilizadas nas questões.
   * @type {string[]}
   * @memberof ScreenCustomExamComponent
   */
  public readonly LETTERS: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

  /**
   * Define se haverá gravação de nova
   */
  public saveScore: boolean;

  /**
   * Define nota de corte
   */
  public passingScore: number;

  /**
   * Texto exibido antes da questão.
   * @type {string}
   * @memberof ScreenCustomExamComponent
   */
  public preText: string;

  /**
   * Lista de questões da avaliação.
   * @type {IQuestion[]}
   * @memberof ScreenCustomExamComponent
   */
  private _questions: IQuestion[];

  /**
   * Lista de feedbacks da avaliação.
   * @type {IFeedback[]}
   * @memberof ScreenCustomExamComponent
   */
  private _feedbacks: IFeedback[];

  /**
   * Define se a resposta será exibida após o usuário conferir.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  private _showAnswer: boolean;


  /**
   * Respostas selecionadas pelo usuário.
   * @type {number[]}
   * @memberof ScreenCustomExamComponent
   */
  private _userAnswers: number[];


  /**
   * Posição da questão atual na lista.
   * @private
   * @type {number}
   * @memberof ScreenCustomExamComponent
   */
  private _currentIndex: number;

  /**
   * Define se a opção atual está selecionada.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  private _currentChecked: boolean;

  /**
   * Define se o usuário marcou todas as questões.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  private _checked: boolean;

  /**
   * Define se o feedback da questão está sendo visualizado.
   * @private
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  private _viewingFeedback: boolean;

  /**
   * Recebe nota do usuário do LMS.
   * @private
   * @type {number}
   * @memberof ScreenCustomExamComponent
   */
  private _userScore: number;

  /**
   * Recebe respostas para montar o gabarito
   * @public
   * @type {boolean[]}
   * @memberof ScreenCustomExamComponent
   */
  private answersTemplate: boolean[];


  /**
   * Construtor.
   * @param {TimeoutService} timeoutService Serviços responsáveis por delays na execução.
   * @memberof ScreenCustomExamComponent
   */
  constructor(private timeoutService: TimeoutService, private protocolService: ProtocolService, private hintService: HintService) {
    super();
  }

  /**
   * Formata os dados recebidos na criação.
   * @param {*} value Valores recebidos na criação.
   * @memberof ScreenCustomExamComponent
   */
  parseData(value: any) {
    //Recebendo noda do usáiro do LMS
    this._userScore = this.protocolService.getScore();

    this.preText = value.preText;
    this._questions = value.questions;
    this._feedbacks = value.feedbacks;
    this._showAnswer = value.showAnswer;
    this._userAnswers = [];
    this.answersTemplate = [];
    this.passingScore = value.passingScore;
    this.saveScore = value.saveScore;

    for (let i = 0; i < this._questions.length; i++) {
      this._userAnswers.push(-1);
    }
  }

  /**
   * Inicia a execução do item.
   * @memberof ScreenCustomExamComponent
   */
  start() {
    this._currentIndex = 0;
  }


  /**
   * Posição da questão atual.
   * @readonly
   * @type {number}
   * @memberof ScreenCustomExamComponent
   */
  get currentIndex(): number {
    return this._currentIndex + 1;
  }

  /**
   * Questão em exibição no momento.
   * @readonly
   * @type {IQuestion}
   * @memberof ScreenCustomExamComponent
   */
  get current(): IQuestion {
    return this._currentIndex >= 0 ? this._questions[this._currentIndex] : null;
  }

  /**
   * Feedback de questão que deve ser exibido.
   * @readonly
   * @type {IQuestionFeedback}
   * @memberof ScreenCustomExamComponent
   */
  get currentFeedback(): IQuestionFeedback {
    let option: number = this._userAnswers[this._currentIndex];

    return this._currentIndex >= 0 && this._viewingFeedback ? this._questions[this._currentIndex].optionsFeedbacks[option] : null;
  }

  /**
   * Valida se a questão possui feedback.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  get hasQuestionFeedback(): boolean {
    if (!this.hasOptionSelected) {
      return false;
    }

    let option: number = this._userAnswers[this._currentIndex];

    return this._currentIndex >= 0 && option >= 0 &&
      this.current.optionsFeedbacks && this.current.optionsFeedbacks[option] != null;
  }

  /**
   * Define se a opção atual está selecionada.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  get currentChecked(): boolean {
    return this._currentChecked;
  }

  /**
   * Define se a resposta é exibida para o aluno.
   * @readonly
   * @type {boolean}
   * @memberof ScreenCustomExamComponent
   */
  get showAnswer(): boolean {
    return this._showAnswer;
  }

  /**
   * Feedback da avaliação baseado na nota do aluno.
   * @readonly
   * @type {IFeedback}
   * @memberof ScreenCustomExamComponent
   */
  get feedback(): IFeedback {
    if (!this.hasFinished()) {
      return null;
    }

    let score: number = this.parseScore();
    for (let i = 0; i < this._feedbacks.length; i++) {
      
      const feedback: IFeedback = this._feedbacks[i];

      if (feedback.minScore <= score && score <= feedback.maxScore) {

        if (feedback.canTryAgain) {
          this.hintService.show("Clique em Refazer Avaliação");
        }

        return feedback;
      }
    }

    return null;
  }

  /**
   * Define se alguma opção foi selecionada.
   * @returns {boolean}
   * @memberof ScreenCustomExamComponent
   */
  hasOptionSelected(): boolean {
    return !isNaN(this._userAnswers[this._currentIndex]) && this._userAnswers[this._currentIndex] >= 0;
  }

  /**
   * Define se a opção está selecionada.
   * @param {number} index Posição da opção validada.
   * @returns {boolean}
   * @memberof ScreenCustomExamComponent
   */
  isOptionSelected(index: number): boolean {
    return this._userAnswers[this._currentIndex] == index;
  }

  /**
   * Valida se a opção da posição recebida é a correta.
   * @param {number} index Posição da opção validada.
   * @returns {boolean}
   * @memberof ScreenCustomExamComponent
   */
  isCorrect(i: number): boolean {
    return this._questions[this._currentIndex].correct == i;
  }

  /**
   * Define se o usuário finalizou a avaliação.
   * @returns {boolean}
   * @memberof ScreenCustomExamComponent
   */
  hasFinished(): boolean {
    return this._userAnswers.indexOf(-1) == -1 && this._currentIndex == -1 && this._checked;
  }


  /**
   * Seleciona a opção da posição recebida.
   * @param {number} index
   * @memberof ScreenCustomExamComponent
   */
  select(index: number): void {
    if (this._currentIndex < 0 || this.currentChecked) {
      return;
    }

    this._userAnswers[this._currentIndex] = this._userAnswers[this._currentIndex] !== index ? index : -1;
  }

  /**
   * Confere a opção e navega para a próxima questão.
   * @memberof ScreenCustomExamComponent
   */
  check(): void {
    if (!this.hasOptionSelected()) {
      return;
    }

    this._currentChecked = true;

    if (!this._showAnswer) {
      if (this.hasQuestionFeedback) {
        this.goFeedback();
      } else {
        this.goNext();
      }
    }
  }

  goFeedback(): void {
    if (this.hasQuestionFeedback) {
      this._viewingFeedback = true;
    }
  }

  goNext(): void {
    let tempCurrentIndex: number = this._currentIndex;
    this._viewingFeedback = false;
    this._currentIndex = -1;

    this.timeoutService.set(() => {
      if (tempCurrentIndex < this._questions.length - 1) {
        this._currentIndex = tempCurrentIndex + 1;
        this._currentChecked = false;
      } else {
        this._checked = true;

        // this.finish();
      }
    }, 300);
  }

  /**
   * Reinicia a avaliação.
   * @memberof ScreenCustomExamComponent
   */
  review(): void {
    this.hintService.hide();

    window["reviewScreen"]();
  }


  /**
   * Calcula a nota do usuário na avaliação.
   * @private
   * @returns {number} Nota do usuário.
   * @memberof ScreenCustomExamComponent
   */
  private parseScore(): number {
    let corrects: number = 0;

    for (let i = 0; i < this._questions.length; i++) {
      const question: IQuestion = this._questions[i];

      this.answersTemplate[i] = question.correct == this._userAnswers[i];
      
      if (this.answersTemplate[i]) {
        corrects++;
      }
    }
    
    let score = Math.round(corrects / this._questions.length * 100);

    if (this.saveScore && environment.production) {
      if (score > this._userScore) {
        this.protocolService.setScore(score);
      }
    }

    if (score >= this.passingScore && this.saveScore) {
      this.finish();
    }

    return score;
  }
}