import { Component, OnInit, HostListener } from '@angular/core';
import { IntroJs } from 'intro.js';
import * as introJs from 'intro.js/intro.js';

import { fadeAnimation } from './shared/animations/animations';
import { HintService } from './shared/services/hint.service';
import { ProtocolService } from './shared/services/protocol.service';
import { StructureService } from './shared/services/structure.service';
import { environment } from 'src/environments/environment';
import { AudioService } from './shared/services/audio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})

/**
 * Componente base para renderização do curso.
 * @export
 * @class AppComponent
 */
export class AppComponent implements OnInit {
  /**
   * Elemento utilizado para dar um delay na exibição do conteúdo.
   * @type {boolean}
   * @memberof AppComponent
   */
  courseLoaded: boolean;


  /**
   * Define se o menu está aberto.
   * @type {boolean}
   * @memberof AppComponent
   */
  menuOpened: boolean;

  /**
   * Define se o tutorial está aberto.
   * @type {boolean}
   * @memberof AppComponent
   */
  tutorialOpened: boolean;

  /**
   * Define se o áudio está ativo.
   * @type {boolean}
   * @memberof AppComponent
   */
  audioActive: boolean;


  /**
   * Construtor.
   * @param {AudioService} audioService Serviços responsáveis pela reprodução de áudios.
   * @param {HintService} hintService Serviços responsáveis por renderizar mensagens de orientação.
   * @param {ProtocolService} protocolService Serviços responsáveis pela comunicação com a plataforma LMS.
   * @param {StructureService} structureService Serviços responsáveis por disponibilizar a estrutura do curso.
   * @memberof AppComponent
   */
  constructor(public audioService: AudioService, public hintService: HintService, public protocolService: ProtocolService,
    public structureService: StructureService) {

  }


  ngOnInit(): void {
    this.audioActive = this.audioService.isActive;

    if (!environment.production) {
      this.courseLoaded = true;
    }
  }


  /**
   * Inicia a execução do curso.
   * @memberof AppComponent
   */
  startCourse(): void {
    this.courseLoaded = true;
  }


  /**
   * Alterna o status de abertura do menu.
   * @memberof AppComponent
   */
  toggleMenuOpened(): void {
    this.menuOpened = !this.menuOpened;
  }

  /**
   * Alterna o status de abertura do tutorial.
   * @memberof AppComponent
   */
  toggleTutorialOpened(): void {
    this.tutorialOpened = !this.tutorialOpened;

    // let tour: IntroJs = introJs();

    // tour.setOption("prevLabel", "Voltar");
    // tour.setOption("nextLabel", "Prosseguir");
    // tour.setOption("skipLabel", "Fechar");
    // tour.setOption("doneLabel", "Fechar");
    // tour.setOption("showStepNumbers", false);
    // tour.start();
  }

  /**
   * Ativa/Desativa o áudio do curso.
   * @memberof AppComponent
   */
  toggleAudio(): void {
    this.audioService.toggleIsActive();

    this.audioActive = this.audioService.isActive;
  }

  // Desabilitando o clique do botão direito do mouse
  @HostListener('contextmenu', ['$event'])
  onRightClick(event) {
    event.preventDefault();
  }
}
