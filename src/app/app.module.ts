import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CoverComponent } from './shared/components/cover/cover.component';
import { LayoutMenuComponent } from './shared/components/layout-menu/layout-menu.component';
import { LayoutTutorialComponent } from './shared/components/layout-tutorial/layout-tutorial.component';
import { ScreenAlternatePopupComponent } from './shared/components/screen-alternate-popup/screen-alternate-popup.component';
import { ScreenAlternateComponent } from './shared/components/screen-alternate/screen-alternate.component';
import { ScreenCharBalloonComponent } from './shared/components/screen-char-balloon/screen-char-balloon.component';
import { ScreenContentComponent } from './shared/components/screen-content/screen-content.component';
import { ScreenCustomExamComponent } from './shared/components/screen-custom-exam/screen-custom-exam.component';
import { ScreenCustomQuestionInputComponent } from './shared/components/screen-custom-question-input/screen-custom-question-input.component';
import { ScreenCustomQuestionJoinColumnsComponent } from './shared/components/screen-custom-question-join-columns/screen-custom-question-join-columns.component';
import { ScreenCustomQuestionMultiSelectComponent } from './shared/components/screen-custom-question-multi-select/screen-custom-question-multi-select.component';
import { ScreenCustomQuestionObjectiveComponent } from './shared/components/screen-custom-question-objective/screen-custom-question-objective.component';
import { ScreenCustomQuestionSelectComponent } from './shared/components/screen-custom-question-select/screen-custom-question-select.component';
import { ScreenCustomQuestionTrueOrFalseComponent } from './shared/components/screen-custom-question-true-or-false/screen-custom-question-true-or-false.component';
import { ScreenImageComponent } from './shared/components/screen-image/screen-image.component';
import { ScreenInnerContainerComponent } from './shared/components/screen-inner-container/screen-inner-container.component';
import { ScreenIntrojsComponent } from './shared/components/screen-introjs/screen-introjs.component';
import { ScreenVideoComponent } from './shared/components/screen-video/screen-video.component';
import { ScreenComponent } from './shared/components/screen/screen.component';
import { ViewHostDirective } from './shared/directives/view-host.directive';
import { SafeHtmlPipe } from './shared/pipes/safe-html.pipe';
import { SafeStylePipe } from './shared/pipes/safe-style.pipe';
import { HintService } from './shared/services/hint.service';
import { ProtocolService } from './shared/services/protocol.service';
import { StructureService } from './shared/services/structure.service';
import { TimeoutService } from './shared/services/timeout.service';


export function getBaseLocation(protocolService: ProtocolService, structureService: StructureService) {
  protocolService.initialize();
  structureService.initializeWith(location.href);
  
  return location.pathname.replace('/index.html', '');
}

@NgModule({
  declarations: [
    AppComponent,

    CoverComponent,
    LayoutMenuComponent,
    LayoutTutorialComponent,
    ScreenComponent,

    ScreenAlternateComponent,
    ScreenAlternatePopupComponent,
    ScreenCharBalloonComponent,
    ScreenContentComponent,
    ScreenImageComponent,
    ScreenInnerContainerComponent,
    ScreenIntrojsComponent,
    ScreenVideoComponent,

    ScreenCustomExamComponent,
    ScreenCustomQuestionInputComponent,
    ScreenCustomQuestionJoinColumnsComponent,
    ScreenCustomQuestionMultiSelectComponent,
    ScreenCustomQuestionObjectiveComponent,
    ScreenCustomQuestionSelectComponent,
    ScreenCustomQuestionTrueOrFalseComponent,

    ViewHostDirective,

    SafeHtmlPipe,
    SafeStylePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([])
  ],
  providers: [
    { provide: APP_BASE_HREF, deps: [ProtocolService, StructureService], useFactory: getBaseLocation },
    HintService,
    ProtocolService,
    StructureService,
    TimeoutService
  ],
  entryComponents: [
    ScreenAlternateComponent,
    ScreenAlternatePopupComponent,
    ScreenCharBalloonComponent,
    ScreenContentComponent,
    ScreenImageComponent,
    ScreenInnerContainerComponent,
    ScreenIntrojsComponent,
    ScreenVideoComponent,

    ScreenCustomExamComponent,
    ScreenCustomQuestionInputComponent,
    ScreenCustomQuestionJoinColumnsComponent,
    ScreenCustomQuestionMultiSelectComponent,
    ScreenCustomQuestionObjectiveComponent,
    ScreenCustomQuestionSelectComponent,
    ScreenCustomQuestionTrueOrFalseComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
